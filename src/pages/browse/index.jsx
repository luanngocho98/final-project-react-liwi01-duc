import * as React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import FeaturedGame from "../../components/browse-content/featured-game";
import TopDownloaded from "../../components/browse-content/top-downloaded";
import HowToStartLiveStream from "../../components/browse-content/start-live";
import MostPopularLiveStream from "../../components/browse-content/livestream";

export default function Browse() {
  return (
    <Box>
      <Grid
        item
        container
        spacing={2}
        sx={{
          justifyContent: "space-between",
          margin: "0px 0px",
        }}
      >
        <FeaturedGame />
        <TopDownloaded />
      </Grid>
      <HowToStartLiveStream />
      <MostPopularLiveStream />
    </Box>
  );
}
