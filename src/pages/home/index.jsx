import React from "react";
import "./index.css";
import { Box } from "@mui/system";
import YourGamingLibrary from "../../components/your-gaming-library";
import MostPopularCard from "../../components/home-content/mostpopularhome";
import BannerHome from "../../components/home-content/bannerhome";
export default function HomePage() {
  return (
    <Box>
      <BannerHome/>
      <MostPopularCard/>
      <YourGamingLibrary/>
    </Box>
  );
}
