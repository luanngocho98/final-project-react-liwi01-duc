import { useTheme } from '@mui/material';
import React, { Fragment } from 'react'
import { Container } from '@mui/system';
import FortNiteDetails from '../../components/details/fortnite-details/name';
import ImageAndContent from '../../components/details/fortnite-details/image';
import OtherGames from '../../components/details/other-game';
import BannerDetails from '../../components/details/bannerdetails';
export default function Details() {
  const theme = useTheme()
  return (
    <Fragment>
      <BannerDetails/>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
        }}
      >
        <FortNiteDetails/>
        <ImageAndContent/>
      </Container>
      <OtherGames />
    </Fragment>
  )
}

