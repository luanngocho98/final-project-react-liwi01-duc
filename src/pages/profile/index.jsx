import { Container, useTheme } from "@mui/material";
import React from "react";
import "./index.css";
import { Box } from "@mui/system";
import ProfileDetails from "../../components/content-profile/profile-details";
import YourGamingLibrary from "../../components/your-gaming-library";
import ContentBox from "../../components/content-profile/content-box";
export default function Profile() {
  const theme = useTheme();
  return (
    <Box>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
        }}
      >
        <ContentBox />
        <ProfileDetails />
      </Container>
      <YourGamingLibrary />
    </Box>
  );
}
