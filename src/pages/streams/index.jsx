import {
  Box,
  Grid,
} from "@mui/material";
import React from "react";
import LiveStreamDone from "../../components/stream/live-stream";
import TopStreamers from "../../components/stream/top-streams/topstream";
import LiveInStreamPage from "../../components/stream/popular-live";

export default function Streams() {
  return (
    <Box>
      <Grid
        item
        container
        spacing={2}
        sx={{
          justifyContent: "space-between",
          margin: "0px 0px",
        }}
      >
        <LiveStreamDone />
        <TopStreamers />
      </Grid>
      <LiveInStreamPage />
    </Box>
  );
}
