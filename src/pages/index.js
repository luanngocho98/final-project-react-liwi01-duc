import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import Router from '../router'
import { ThemeProvider } from '@mui/material/styles';
import { theme } from '../theme';
export default function Main() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <Router />
      </ThemeProvider>
    </BrowserRouter>
  )
}
