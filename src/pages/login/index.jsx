import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Logo from '../../helper/image/logo.png'
import { useState } from 'react';
import { Box } from '@mui/system';
import TextField from '@mui/material/TextField';
import GoogleIcon from '@mui/icons-material/Google';
import { FormControl, InputAdornment, InputLabel, OutlinedInput } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { useTheme } from "@mui/material";
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { LOGIN_ACTIONS } from '../../actions';
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

export function BootstrapDialogTitle() {
  const [onClose, setOnCloses] = useState(false);
  const theme = useTheme()
  return (
    <DialogTitle 
      sx={{ 
        m: 0,
        p: 2, 
        textAlign: 'center', 
      }}
    >
      <Box>
        <img src={Logo} alt='Cyborg Logo'/>
      </Box>
      <Box
        sx={{
          display: 'flex',
          alignItems:'center',
          justifyContent:'center',
          
        }}
      >
        <Typography 
          sx={{
            fontSize: '24px',
            color: theme.palette.primary.light
          }}
        >
          Chào Mừng Bạn Đến Với
        </Typography>
        <Typography
          sx={{
            fontSize: '24px',
            fontWeight: 600,
            fontStyle: 'italic',
            ml: 1,
            color: theme.palette.primary.lighter
          }}
        >
          CYBORG
        </Typography>
      </Box>
      {/* {onClose ? (
        <IconButton aria-label="close" onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null} */}
    </DialogTitle>
  );
}

export default function LoginButton() {
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();
  const location = useLocation()
  const [showPassword, setShowPassword] = useState(false);
  const theme = useTheme();
  const { isAuthen } = useSelector((state) => state.authen);
  const handleClickShowPassword = () => setShowPassword(showPassword => !showPassword);
  const dispatch = useDispatch();

  const handleClickOpen = () => {
    dispatch({ type: LOGIN_ACTIONS.OPEN });
  };
  const handleClose = () => {
    setOpen(false);
    if(!isAuthen) {
      
    }
  };
  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Login
      </Button>
      {/* <BootstrapDialog
        onClose={handleClose}
        open={open}
        sx={{
          '& .MuiPaper-root' : {
            backgroundColor: theme.palette.primary.darker
          }
        }}
      >
        <BootstrapDialogTitle
          onClose={handleClose}
        >
        </BootstrapDialogTitle>
        <DialogContent 
          dividers 
          sx={{
            textAlign: 'center',
          }}
        >
          <Box
            m={1}
          >
            <TextField 
              id="outlined-basic" 
              label='Email' 
              variant="outlined"
              sx={{
                width: '100%',
                '& .MuiOutlinedInput-root' : {
                  borderRadius: '50px'
                },
                '& .MuiFormLabel-root': {
                  color: theme.palette.primary.main
                },
                '& .MuiOutlinedInput-input': {
                  color: theme.palette.primary.light
                }
              }}
            />
          </Box>
          <Box
            m={1}
          >
            <FormControl 
              sx={{ 
                width: '100%',
                '& .MuiOutlinedInput-root' : {
                  borderRadius: '50px',
                },
                '& .MuiFormLabel-root': {
                  color: theme.palette.primary.main
                },
                '& .MuiOutlinedInput-input': {
                  color: theme.palette.primary.light
                }
              }} 
              variant="outlined"
            >
              <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      edge="end"
                    >
                      {showPassword ? <Visibility sx={{color: theme.palette.primary.lighter}} /> : <VisibilityOff sx={{color: theme.palette.primary.lighter}}/>}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
              />
            </FormControl>
          </Box>
          <Box
            sx={{
              textAlign: 'end'
            }}
          >
            <Typography 
              sx={{
                fontWeight: 500,
                fontSize: '16px',
                '&:hover' : {
                  cursor: 'pointer'
                },
                color: theme.palette.primary.main
              }}
            >
              Quên mật khẩu ?
            </Typography>
          </Box>
          <Box m={1}>
            <Button
              sx={{
                fontSize: theme.typography.button.fontSize,
                fontWeight: theme.typography.button.fontWeight,
                color: theme.palette.primary.light,
                backgroundColor: theme.palette.primary.lighter,
                borderRadius: theme.shape.borderRadius,
                padding: "12px 30px",
                border: "none",
                width: '100%'
              }}
              autoFocus onClick={handleClose}
            >
              Đăng nhập
            </Button>
          </Box>
          <Box m={1}>
            <Button
              sx={{
                fontSize: theme.typography.button.fontSize,
                fontWeight: theme.typography.button.fontWeight,
                color: theme.palette.primary.lighter,
                backgroundColor: theme.palette.primary.light,
                borderRadius: theme.shape.borderRadius,
                padding: "12px 30px",
                border: "none",
                width: '100%',
                textTransform: 'unset'
              }}
            >
              <GoogleIcon/>
              <Typography pl={1}>
                Tiếp tục với Gmail
              </Typography>
            </Button>
          </Box>
          <Box>
            <Box mb={1}>
              <Typography sx={{textAlign: 'center', color: theme.palette.primary.main, fontSize:'15px'}}>
                Bằng cách tiếp tục, bạn đồng ý với các
              </Typography>
              <Typography sx={{display: 'flex',justifyContent: 'center'}}>
                  <Typography sx={{color: theme.palette.primary.light,fontSize:'15px', fontWeight: 500, pr: 1, '&:hover': {cursor: 'pointer'}}}>
                    Điều khoản dịch vụ
                  </Typography>
                  <Typography sx={{color: theme.palette.primary.main,fontSize:'15px'}}>
                    của chúng tôi
                  </Typography>
              </Typography>
            </Box>
            <Typography mb={1} sx={{display: 'flex', justifyContent: 'center'}}>
                <Typography sx={{color: theme.palette.primary.main,fontSize:'15px'}}>
                  Bạn chưa tham gia Mekan?
                </Typography>
                <Typography sx={{color: theme.palette.primary.light, fontWeight: 500,fontSize:'15px', pl: 1,'&:hover': {cursor: 'pointer'}}}>
                  Đăng ký
                </Typography>
            </Typography>
            <Typography sx={{display: 'flex', justifyContent: 'center'}}>
                <Typography sx={{color: theme.palette.primary.main,fontSize:'15px'}}>
                  Bạn là doanh nghiệp? Hãy bắt đầu
                </Typography>
                <Typography sx={{color: theme.palette.primary.light, fontWeight: 500,fontSize:'15px', pl: 1,'&:hover': {cursor: 'pointer'}}}>
                  tại đây!
                </Typography>
            </Typography>
          </Box>
        </DialogContent>
      </BootstrapDialog> */}
    </div>
  );
}
