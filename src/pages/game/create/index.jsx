import { Box, Button, Grid, IconButton, Rating, Typography, useTheme } from '@mui/material'
import React, { useMemo, useState } from 'react'
import Selection from '../../../components/Selection';
import PublicIcon from '@mui/icons-material/Public';
import VpnLockIcon from '@mui/icons-material/VpnLock';
import TextFieldCustom from '../../../components/TextfieldCustom';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { SNACKBAR_ACTIONS } from '../../../actions';

export default function GameCreate() {
  const theme = useTheme();
  const dispatch = useDispatch();
  const methods = useForm({
    mode: 'onSubmit',
    defaultValues: {
      title: '',
      description: '',
      storage: '',
      download: 0,
      imageUrl: '',
      rate: '2.5',
    }
  });
  const {
    handleSubmit,
    control,
    formState: { errors },
    setValue,
    reset,
  } = methods;
  const [imageLink, setImageLink] = useState('');
  const status = useMemo(() => {
    return [
      {
        name: 'Công khai',
        value: 'public',
        icon: <PublicIcon />,
        isDefault: true,
      },
      {
        name: 'Riêng tư',
        value: 'private',
        icon: <VpnLockIcon />,
      }
    ]
  }, []);
  
  const handleBlurInput = (value) => {
    setImageLink(value);
  };

  const submit = (data) => {
    console.log(data, '----data');
  };

  const clearImage = () => {
    setImageLink('');
    setValue('imageUrl', '');
  };

  const error = () => {
    dispatch({ type: SNACKBAR_ACTIONS.OPEN, message: 'Vui lòng kiểm tra dữ liệu' });
  };

  return (
    <form onSubmit={handleSubmit(submit, error)}>
      <Box>
        <Grid container justifyContent="space-between">
          <Grid item sm={6}>
            <Typography
              fontSize="32px"
              color={theme.palette.primary.lighter}
            >
              Tạo mới Game
            </Typography>
          </Grid>
          <Grid item sm={2.5}>
            <Selection
              name="isPublic"
              options={status}
              control={control}
              errors={errors}
              isRequired
            />
          </Grid>
        </Grid>
        <Box
          sx={{
            '& .MuiOutlinedInput-notchedOutline': {
              border: `1px solid ${theme.palette.secondary.lighter}`,
            },
            '& input, textarea': {
              color: theme.palette.secondary.lighter,
            }
          }}
          mt={3}
        >
          <Box>
            <Typography
              fontSize="17px"
              color={theme.palette.secondary.lighter}
              mb={0.5}
            >
              Tên game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="title"
              placeholder="Nhập tên game"
              control={control}
              errors={errors}
              isRequired
            />
          </Box>
          <Box mt={3}>
            <Typography
              fontSize="17px"
              color={theme.palette.secondary.lighter}
              mb={0.5}
            >
              Mô tả game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="description"
              placeholder="Nhập mô tả game"
              type="textarea"
              rows={5}
              control={control}
              errors={errors}
              isRequired
            />
          </Box>
          <Box mt={3}>
            <Grid container justifyContent="space-between">
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.secondary.lighter}
                  mb={0.5}
                >
                  Số lượng downloaded
                </Typography>
                <TextFieldCustom
                  fullWidth
                  name="download"
                  disabled
                  control={control}
                  errors={errors}
                  defaultValue={0}
                />
              </Grid>
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.secondary.lighter}
                  mb={0.5}
                >
                  Dung lượng game
                </Typography>
                <TextFieldCustom
                  fullWidth
                  name="storage"
                  placeholder="Nhập dung lượng"
                  control={control}
                  errors={errors}
                  isRequired
                />
              </Grid>
              <Grid item sm={3}>
                <Typography
                  fontSize="17px"
                  color={theme.palette.secondary.lighter}
                  mb={0.5}
                >
                  Số sao đánh giá
                </Typography>
                <Rating name="half-rating-read" defaultValue={2.5} precision={0.5} readOnly />
              </Grid>
            </Grid>
          </Box>
          <Box mt={3}>
            <Typography
              fontSize="17px"
              color={theme.palette.secondary.lighter}
              mb={0.5}
            >
              Hình ảnh game
            </Typography>
            <TextFieldCustom
              fullWidth
              name="imageUrl"
              placeholder="Nhập mô tả game"
              control={control}
              errors={errors}
              isRequired
              onBlur={handleBlurInput}
            />
          </Box>
          <Box mt={3}>
            {imageLink && (
              <Box
                sx={{
                  position: 'relative',
                }}
              >
                <img width="100%" alt="image link render" src={imageLink} />
                <IconButton
                  sx={{
                    position: 'absolute',
                    top: '5px',
                    right: '5px',
                  }}
                  onClick={clearImage}
                >
                  <HighlightOffIcon sx={{ color: theme.palette.secondary.lighter }} />
                </IconButton>
              </Box>
            )}
          </Box>
          <Box mt={3} textAlign="end">
            <Button
              variant="contained"
              type="submit"
              sx={{
                bgcolor: 'white',
                mr: 2,
              }}
            >
              Save
            </Button>
            <Button
              variant="contained"
              sx={{
                bgcolor: 'grey',
              }}
              onClick={() => {
                reset();
                setImageLink('');
              }}
            >
              Clear
            </Button>
          </Box>
        </Box>
      </Box>
    </form>
  )
}
