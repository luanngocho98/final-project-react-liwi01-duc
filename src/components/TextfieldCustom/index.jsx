import { IconButton, InputAdornment, TextField } from '@mui/material'
import React, { Fragment, useState } from 'react'
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { Controller } from 'react-hook-form';

export default function TextFieldCustom(props) {
  const {
    placeholder,
    name,
    onChange,
    type = 'text',
    fullWidth = false,
    isStartIcon,
    iconStart,
    isEndIcon,
    iconEnd,
    disabled = false,
    defaultValue,
    rows,
    onBlur,
    control,
    isRequired = false,
  } = props;
  const [isOpenPassword, setIsOpenPassword] = useState(false);
  const handleChangeStatusIconPassword = () => {
    setIsOpenPassword(!isOpenPassword);
  };

  const renderInputFollowType = (type) => {
    switch(type) {
      case 'text':
        return (
          <Controller
            name={name}
            control={control}
            rules={{
              required: isRequired ? `Field ${name} này là bắt buộc nhập` : '',
            }}
            defaultValue={defaultValue}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  placeholder={placeholder}
                  disabled={disabled}
                  type={type}
                  fullWidth={fullWidth}
                  value={field.value}
                  autoComplete="off"
                  helperText={fieldState?.error?.message || ''}
                  sx={{
                    '& .MuiInputBase-root': {
                      '& input': {
                        height: '16px',
                        fontSize: '16px',
                      }
                    },
                    '& .MuiFormHelperText-root': {
                      color: 'red',
                    }
                  }}
                  onBlur={() => onBlur?.(field.value)}
                  InputProps={{
                    startAdornment: isStartIcon ? (
                      <InputAdornment position="start">
                        {iconStart}
                      </InputAdornment>
                    ) : (''),
                    endAdornment: isEndIcon ? (
                      <InputAdornment position="end">
                        {iconEnd}
                      </InputAdornment>
                    ) : (''),
                  }}
                />
              );
            }}
          />
        )
      case 'textarea':
        return (
          <Controller
            name={name}
            control={control}
            rules={{
              required: isRequired ? `Field ${name} này là bắt buộc nhập` : '',
            }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  multiline
                  rows={rows}
                  placeholder={placeholder}
                  disabled={disabled}
                  type={type}
                  value={field.value}
                  fullWidth={fullWidth}
                  autoComplete="off"
                  helperText={fieldState?.error?.message || ''}
                  sx={{
                    '& .MuiInputBase-root': {
                      '& textarea': {
                        height: '16px',
                        fontSize: '16px',
                      }
                    },
                    '& .MuiFormHelperText-root': {
                      color: 'red',
                    }
                  }}
                  InputProps={{
                    startAdornment: isStartIcon ? (
                      <InputAdornment position="start">
                        {iconStart}
                      </InputAdornment>
                    ) : (''),
                    endAdornment: isEndIcon ? (
                      <InputAdornment position="end">
                        {iconEnd}
                      </InputAdornment>
                    ) : (''),
                  }}
                />
              );
            }}
          />
        )
      case 'password':
        return (
          <Controller
            name={name}
            control={control}
            rules={{
              required: isRequired ? `Field ${name} này là bắt buộc nhập` : '',
            }}
            render={({ field, fieldState }) => {
              return (
                <TextField
                  {...field}
                  placeholder={placeholder}
                  disabled={disabled}
                  name={name}
                  value={field.value}
                  type={!isOpenPassword ? 'password' : 'text'}
                  fullWidth={fullWidth}
                  helperText={fieldState?.error?.message || ''}
                  sx={{
                    '& .MuiInputBase-root': {
                      '& input': {
                        height: '16px',
                        fontSize: '16px',
                      }
                    },
                    '& .MuiFormHelperText-root': {
                      color: 'red',
                    }
                  }}
                  InputProps={{
                    startAdornment: isStartIcon ? (
                      <InputAdornment position="start">
                        {iconStart}
                      </InputAdornment>
                    ) : (''),
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={handleChangeStatusIconPassword}>
                        {isOpenPassword ? (<VisibilityIcon />) : (<VisibilityOffIcon />)}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              );
            }}
          />
        )
      default:
        return '';
    }
  }
  return (
    <Fragment>
      {renderInputFollowType(type)}
    </Fragment>
  )
}
