import { Box, Button, Dialog, IconButton, Typography } from '@mui/material';
import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import { LOGIN_ACTIONS, SNACKBAR_ACTIONS } from '../../../actions';
import CloseIcon from '@mui/icons-material/Close';
import Logo from '../../../helper/image/logo.png';
import TextFieldCustom from '../../TextfieldCustom';
import EmailIcon from '@mui/icons-material/Email';
import NoEncryptionIcon from '@mui/icons-material/NoEncryption';
import { useForm } from 'react-hook-form';

export default function AuthenticationDialog() {
  const { isOpen } = useSelector((state) => state.login);
  const dispatch = useDispatch();
  const methods = useForm({
    mode: 'onSubmit',
    defaultValues: {
      email: '',
      password: '',
    }
  });
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = methods;
  const handleClose = () => {
    dispatch({ type: LOGIN_ACTIONS.CLOSE });
  };
  const submit = (data) => {
    console.log(data, '----data');
  };
  const error = () => {
    dispatch({ type: SNACKBAR_ACTIONS.OPEN, message: 'Vui lòng kiểm tra dữ liệu' });
  };
  return (
    <Dialog
      onClose={handleClose}
      open={isOpen}
      sx={{
        '& .MuiPaper-root': {
          padding: '24px',
          minWidth: '520px',
        },
      }}
    >
      <form onSubmit={handleSubmit(submit, error)}>
        <Box textAlign="end">
          <IconButton onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </Box>
        <Box textAlign="center">
          <img src={Logo} alt='Cyborg Logo'/>
        </Box>
        <Typography
          fontSize="30px"
          textAlign="center"
          mt={1}
          sx={{
            '& span': {
              fontStyle: 'italic'
            }
          }}
        >
          Chào mừng bạn đến với <span>Cyborg</span>
        </Typography>
        <Box px={6}>
          <Box mt={1}>
            <TextFieldCustom
              name="email"
              placeholder="Email"
              type="text"
              fullWidth
              isStartIcon
              iconStart={<EmailIcon />}
              control={control}
              isRequired
            />
          </Box>
          <Box mt={2}>
            <TextFieldCustom
              name="password"
              placeholder="Password"
              type="password"
              fullWidth
              isStartIcon
              iconStart={<NoEncryptionIcon />}
              control={control}
              isRequired
            />
          </Box>
          <Button type="submit" variant="outlined" sx={{ mt: 2, }}>
            Login
          </Button>
        </Box>
      </form>
    </Dialog>
  )
}
