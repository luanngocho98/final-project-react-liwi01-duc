import { Button, Grid, Typography, useTheme } from "@mui/material";

import React, { Fragment, useState } from "react";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import ArrowRightSharpIcon from "@mui/icons-material/ArrowRightSharp";
import { useSelector } from "react-redux";
import { Box } from "@mui/system";
export default function ProfileDetails() {
  const [viewMoreProfileDetails, setViewMoreProfileDetails] = useState(false);
  const { profileDetails } = useSelector((state) => state.profile);
  const { profileDetailsviewall } = useSelector(
    (state) => state.profile
  );
  const theme = useTheme();
  return (
    <Fragment>
      <Box
        sx={{
          display: "flex",
          borderTop: `2px solid ${theme.palette.secondary.dark}`,
          padding: "30px 0px",
        }}
      >
        <Typography
          variant="h2"
          sx={{
            color: theme.palette.primary.light,
            textDecoration: "underline",
          }}
        >
          Your Most Popular
        </Typography>
        <Typography
          variant="h2"
          sx={{
            color: theme.palette.primary.lighter,
            marginLeft: "15px",
          }}
        >
          Clips
        </Typography>
      </Box>
      {viewMoreProfileDetails ? (
        <Grid
          container
          justifyContent="space-between"
          sx={{
            overflowY: "scroll",
            height: "450px",
            "::-webkit-scrollbar": {
              width: "5px",
            },
            "::-webkit-scrollbar-track": {
              background: theme.palette.primary.main,
            },
            "::-webkit-scrollbar-thumb ": {
              background: theme.palette.primary.lighter,
            },
            "::-webkit-scrollbar-thumb": {
              "&:hover": {
                background: theme.palette.secondary.contrastText,
              },
            },
          }}
        >
          {profileDetailsviewall.map((profileDetail) => {
            return (
              <Grid
                key={profileDetail.id}
                item
                sm={2.85}
                bgcolor={theme.palette.primary.darker}
                mb={2.5}
                p={"24px 18px"}
                borderRadius="24px"
              >
                <Grid
                  item
                  sx={{
                    "& img": {
                      borderRadius: "24px",
                      width: "100%",
                    },
                  }}
                >
                  <img alt="Profile Avatar" src={profileDetail.picture} />
                  <Grid
                    item
                    container
                    alignItems="center"
                    sx={{
                      "& a": {
                        // transform: translate(-23px, -23px),
                        width: "46px",
                        height: "46px",
                        backgroundColor: "#fff",
                        borderRadius: "50%",
                        color: "#ec6090",
                        marginTop: "-78%",
                        marginLeft: "38%",
                      },
                    }}
                  >
                    <a
                      href="https://www.youtube.com/watch?v=r1b03uKWk_M"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <ArrowRightSharpIcon
                        sx={{
                          fontSize: "48px",
                        }}
                      />
                    </a>
                  </Grid>
                </Grid>
                <Grid container mt={3} alignItems="center">
                  <Grid item sm={7}>
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {profileDetail.name}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    sm={5}
                    alignItems="center"
                    justifyContent="flex-end"
                  >
                    <RemoveRedEyeIcon
                      sx={{ color: theme.palette.primary.lighter, mr: 0.5 }}
                    />
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {profileDetail.quality}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
          <Box
            sx={{
              textAlign: "center",
              paddingBottom: "30px",
            }}
          >
            <Button
              sx={{
                bgcolor: theme.palette.primary.lighter,
                color: theme.palette.secondary.lighter,
                minHeight: "48.5px",
                borderRadius: theme.shape.borderRadius,
                padding: "12px 30px",
                marginTop: "24px",
                "&:hover": {
                  bgcolor: theme.palette.secondary.lighter,
                  color: theme.palette.primary.lighter,
                },
                display: viewMoreProfileDetails ? "none" : "unset",
              }}
              onClick={() => setViewMoreProfileDetails(!viewMoreProfileDetails)}
            >
              Load More Clips
            </Button>
          </Box>
        </Grid>
      ) : (
        <Grid item container justifyContent="center">
          <Grid container justifyContent="space-between">
            {profileDetails.map((profileDetail) => {
              return (
                <Grid
                  key={profileDetail.id}
                  item
                  sm={2.85}
                  bgcolor={theme.palette.primary.darker}
                  mb={2.5}
                  pl={2}
                  p={"24px 18px"}
                  borderRadius="24px"
                >
                  <Grid
                    item
                    sx={{
                      "& img": {
                        borderRadius: "24px",
                        width: "100%",
                      },
                    }}
                  >
                    <img alt="ProfilePicture" src={profileDetail.picture} />
                    <Grid
                      item
                      container
                      alignItems="center"
                      sx={{
                        "& a": {
                          // transform: translate(-23px, -23px),
                          width: "46px",
                          height: "46px",
                          backgroundColor: "#fff",
                          borderRadius: "50%",
                          color: "#ec6090",
                          marginTop: "-78%",
                          marginLeft: "38%",
                        },
                      }}
                    >
                      <a
                        href="https://www.youtube.com/watch?v=r1b03uKWk_M"
                        target="_blank"
                        rel="noreferrer"
                      >
                        <ArrowRightSharpIcon
                          sx={{
                            fontSize: "48px",
                          }}
                        />
                      </a>
                    </Grid>
                  </Grid>
                  <Grid container mt={3} alignItems="center">
                    <Grid item sm={7}>
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.lighter}
                      >
                        {profileDetail.name}
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      container
                      sm={5}
                      alignItems="center"
                      justifyContent="flex-end"
                    >
                      <RemoveRedEyeIcon
                        sx={{ color: theme.palette.primary.lighter, mr: 0.5 }}
                      />
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.lighter}
                      >
                        {profileDetail.quality}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              );
            })}
          </Grid>
          <Box
            sx={{
              textAlign: "center",
              paddingBottom: "30px",
            }}
          >
            <Button
              sx={{
                bgcolor: theme.palette.primary.lighter,
                color: theme.palette.secondary.lighter,
                minHeight: "48.5px",
                borderRadius: theme.shape.borderRadius,
                padding: "12px 30px",
                marginTop: "24px",
                "&:hover": {
                  bgcolor: theme.palette.secondary.lighter,
                  color: theme.palette.primary.lighter,
                },
              }}
              onClick={() => setViewMoreProfileDetails(!viewMoreProfileDetails)}
            >
              Load More Clips
            </Button>
          </Box>
        </Grid>
      )}
    </Fragment>
  );
}
