import { Button, Grid, Typography, useTheme } from "@mui/material";
import { Box } from "@mui/system";
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import ProfilePicture from "../../../helper/image/profile.jpg";
export default function ContentBox() {
  const theme = useTheme();
  const { contentBox } = useSelector((state) => state.contentbox);
  return (
    <Fragment>
      <Grid container justifyContent="space-between" pt={3} pb={3}>
        <Grid
          item
          sm={4.5}
          sx={{
            "& img": {
              borderRadius: "24px",
              width: "100%",
            },
          }}
        >
          <img src={ProfilePicture} alt="ProfilePicTure" />
        </Grid>
        <Grid item sm={3.5}>
          <Button
            sx={{
              borderRadius: theme.shape.borderRadius,
              backgroundColor: theme.palette.primary.lighter,
              color: theme.palette.primary.light,
              padding: "8px 20px",
            }}
          >
            Offline
          </Button>
          <Typography
            sx={{
              color: "white",
              fontSize: "24px",
              fontWeight: 700,
              padding: "20px 0px",
            }}
          >
            Alan Smithee
          </Typography>
          <Box
            sx={{
              color: "#666",
              fontSize: "14px",
            }}
          >
            You Haven't Gone Live yet. Go Live By
          </Box>
          <Box
            sx={{
              color: "#666",
              fontSize: "14px",
              marginTop: "10px",
            }}
          >
            Touching The Button Below.
          </Box>
          <Button
            sx={{
              border: `1px solid ${theme.palette.primary.lighter}`,
              color: theme.palette.primary.lighter,
              padding: "8px 20px",
              borderRadius: theme.shape.borderRadius,
              marginTop: "24px",
            }}
          >
            Start Live Stream
          </Button>
        </Grid>
        <Grid
          item
          sm={3.5}
          sx={{
            borderRadius: theme.shape.borderRadius,
            padding: "24px 24px 0px 24px",
            marginBottom: "32px",
            backgroundColor: theme.palette.primary.darker,
          }}
        >
          {contentBox.map((content) => {
            return (
              <Box
                key={content.id}
                sx={{
                  marginBottom: "20px",
                  paddingBottom: "20px",
                  borderBottom:
                    content.name === "Clips"
                      ? "unset"
                      : `1px solid ${theme.palette.secondary.main}`,
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <Box
                  sx={{
                    fontSize: theme.typography.h6.fontSize,
                    color: theme.palette.primary.main,
                  }}
                >
                  {content.name}
                </Box>
                <Box
                  sx={{
                    fontWeight: 500,
                    color: theme.palette.primary.lighter,
                  }}
                >
                  {content.number}
                </Box>
              </Box>
            );
          })}
        </Grid>
      </Grid>
    </Fragment>
  );
}
