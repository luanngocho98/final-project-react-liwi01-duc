import { Grid, Typography, useTheme } from "@mui/material";
import React, { Fragment } from "react";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";
import { useSelector } from "react-redux";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import Slider from "react-slick";
export default function FeaturedGame() {
  const theme = useTheme();
  const { featuredGames } = useSelector((state) => state.game);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    // slidesToShow: 3,
    // slidesToScroll: 3
  };
  return (
    <Fragment>
      <Grid
        item
        container
        sm={8}
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
        }}
      >
        <Grid
          item
          sm={12}
          sx={{
            display: "flex",
            padding: "16px 0px 0px 10px",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Grid item sx={{ display: "flex" }}>
            <Typography
              variant="h2"
              sx={{
                color: theme.palette.primary.light,
                textDecoration: "underline",
              }}
            >
              Featured
            </Typography>
            <Typography
              variant="h2"
              sx={{
                color: theme.palette.primary.lighter,
                marginLeft: "15px",
              }}
            >
              Games
            </Typography>
          </Grid>
          <Grid
            item
            sx={{
              color: theme.palette.primary.main,
            }}
          >
            <KeyboardArrowLeftIcon
              sx={{
                "&:hover": {
                  color: theme.palette.primary.lighter,
                  cursor: "pointer",
                },
              }}
            />
            <KeyboardArrowRightIcon
              sx={{
                "&:hover": {
                  color: theme.palette.primary.lighter,
                  cursor: "pointer",
                },
              }}
            />
          </Grid>
        </Grid>

        <Grid container justifyContent="center" m={"0px -7px"}>
          {featuredGames.map((featuredGame) => {
            return (
              <Grid key={featuredGame.id} item sm={3.85} p={"16px 8px"}>
                <Grid
                  item
                  container
                  justifyContent="center"
                  sx={{
                    "& img": {
                      width: "100%",
                      borderRadius: theme.shape.borderRadius,
                    },
                    ":hover": {
                      cursor: "pointer",
                      "& .viewer": {
                        position: "relative",
                        bottom: "32px",
                        opacity: 1,
                      },
                    },
                  }}
                >
                  <img src={featuredGame.image} alt="FeaturedGameImage" />
                  <Grid
                    className="viewer"
                    sm={9}
                    sx={{
                      color: theme.palette.primary.lighter,
                      fontWeight: 600,
                      fontSize: "13px",
                      backgroundColor: theme.palette.primary.dark,
                      borderRadius: "24px",
                      textAlign: "center",
                      padding: "10px",
                      marginTop: "-16px",
                      opacity: 0,
                    }}
                  >
                    {featuredGame.viewer}
                  </Grid>
                </Grid>
                <Grid container mt={1.5} alignItems="center">
                  <Grid item sm={7}>
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {featuredGame.mainName}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    sm={5}
                    alignItems="center"
                    justifyContent="flex-end"
                  >
                    <GradeIcon sx={{ color: theme.palette.secondary.darker }} />
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {featuredGame.voteStar}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container alignItems="center">
                  <Grid item sm={7}>
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.main}
                    >
                      {featuredGame.branchName}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    sm={5}
                    alignItems="center"
                    justifyContent="flex-end"
                  >
                    <DownloadIcon
                      sx={{ color: theme.palette.primary.lighter }}
                    />
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {featuredGame.qualityDownload}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Fragment>
  );
}
