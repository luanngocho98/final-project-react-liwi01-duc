import { Box, Button, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export default function HowToStartLiveStream() {
  const theme = useTheme();
  const navigate = useNavigate();
  const { howtostartLiveStream } = useSelector((state) => state.livestream);
  return (
    <Fragment>
      <Box
        sx={{
          textAlign: "start",
          padding: "16px 0px 16px 10px",
          display: "flex",
          justifyContent: "center",
          margin: "32px",
        }}
      >
        <Typography
          variant="h2"
          sx={{
            color: theme.palette.primary.light,
            textDecoration: "underline",
          }}
        >
          How To Start Your
        </Typography>
        <Typography
          variant="h2"
          sx={{
            color: theme.palette.primary.lighter,
            marginLeft: "16px",
          }}
        >
          Live Stream
        </Typography>
      </Box>
      <Grid sx={{ display: "flex", justifyContent: "space-between" }}>
        {howtostartLiveStream.map((howtoLive) => {
          return (
            <Grid
              item
              container
              sm={3.8}
              sx={{
                border: `0.5px solid ${theme.palette.secondary.main}`,
                borderRadius: theme.shape.borderRadius,
                padding: "16px 24px",
                display: "block",
              }}
            >
              <Grid
                item
                sx={{
                  "& img": {
                    borderRadius: "50%",
                  },
                }}
              >
                <img src={howtoLive.image} alt="HowToLiveStreamImage" />
              </Grid>
              <Grid item>
                <Typography
                  variant="h6"
                  color={theme.palette.secondary.lighter}
                  sx={{
                    fontSize: "20px",
                    margin: "8px 0px 8px 0px",
                  }}
                >
                  {howtoLive.mainName}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="h6" color={theme.palette.secondary.main}>
                  {howtoLive.content}
                </Typography>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
      <Box
        sx={{
          textAlign: "center",
          paddingBottom: "30px",
        }}
      >
        <Button
          onClick={() => navigate("/profile")}
          sx={{
            bgcolor: theme.palette.primary.lighter,
            color: theme.palette.secondary.lighter,
            minHeight: "48.5px",
            borderRadius: theme.shape.borderRadius,
            padding: "12px 30px",
            marginTop: "24px",
            "&:hover": {
              bgcolor: theme.palette.secondary.lighter,
              color: theme.palette.primary.lighter,
            },
          }}
        >
          Go To Profiles
        </Button>
      </Box>
    </Fragment>
  );
}
