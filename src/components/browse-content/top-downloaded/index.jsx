import { Typography, useTheme, Grid, Button } from "@mui/material";
import { Box } from "@mui/system";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";
export default function TopDownloaded() {
  const theme = useTheme();
  const [viewallTopdownloaded, setViewAllTopDownloaded] = useState(false);
  const { topDownloaded } = useSelector((state) => state.game);
  const { topDownloadedviewmore } = useSelector(
    (state) => state.game
  );
  return (
    <Fragment>
      <Grid
        item
        sm={3.5}
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginRight: "16px",
        }}
      >
        <Grid
          item
          sm={12}
          sx={{
            padding: "16px 0px 16px 10px",
            display: "flex",
            flexWrap: "wrap",
          }}
        >
          <Typography
            variant="h2"
            mr={2}
            sx={{
              color: theme.palette.primary.light,
              textDecoration: "underline",
            }}
          >
            Top
          </Typography>

          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.lighter,
            }}
          >
            Downloaded
          </Typography>
        </Grid>
        {viewallTopdownloaded ? (
          <Grid
            item
            sx={{
              height: "450px",
              overflowY: "scroll",
              "::-webkit-scrollbar": {
                width: "5px",
              },
              "::-webkit-scrollbar-track": {
                background: theme.palette.primary.main,
              },
              "::-webkit-scrollbar-thumb ": {
                background: theme.palette.primary.lighter,
              },
              "::-webkit-scrollbar-thumb": {
                "&:hover": {
                  background: theme.palette.secondary.contrastText,
                },
              },
            }}
          >
            {topDownloadedviewmore.map((downloaded) => {
              return (
                <Fragment>
                  <Grid
                    key={downloaded.id}
                    item
                    sx={{
                      "& img": {
                        borderRadius: theme.shape.borderRadius,
                        width: "100%",
                      },
                      display: "flex",
                    }}
                  >
                    <Grid item sm={4}>
                      <img src={downloaded.image} alt="DownloadCyborgImage" />
                    </Grid>
                    <Grid
                      item
                      sm={6}
                      sx={{
                        padding: "0px 0px 16px 8px",
                      }}
                    >
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.lighter}
                      >
                        {downloaded.mainName}
                      </Typography>
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.main}
                      >
                        {downloaded.branchName}
                      </Typography>
                      <Grid
                        container
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Grid item container xs={5} alignItems="center">
                          <GradeIcon
                            sx={{ color: theme.palette.secondary.darker }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {downloaded.voteStar}
                          </Typography>
                        </Grid>
                        <Grid item container xs={6} alignItems="center">
                          <DownloadIcon
                            sx={{ color: theme.palette.primary.lighter }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {downloaded.qualityDownload}
                          </Typography>
                        </Grid>
                        <Grid item sm={1}>
                          <Button
                            sx={{
                              backgroundColor: theme.palette.primary.darker,
                              borderRadius: "50%",
                              minWidth: "0px",
                            }}
                          >
                            <DownloadIcon />
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <hr
                    style={{
                      border: `0.5px solid ${theme.palette.secondary.dark}`,
                      margin: "0px 16px 16px 0px",
                    }}
                  />
                </Fragment>
              );
            })}
            <Box textAlign="center">
              <Button
                variant="contained button"
                sx={{
                  bgcolor: theme.palette.primary.lighter,
                  color: theme.palette.secondary.lighter,
                  minHeight: "48.5px",
                  borderRadius: theme.shape.borderRadius,
                  mb: "-20px",
                  "&:hover": {
                    bgcolor: theme.palette.secondary.lighter,
                    color: theme.palette.primary.lighter,
                  },
                  display: viewallTopdownloaded ? "none" : "unset",
                }}
                onClick={() => setViewAllTopDownloaded(!viewallTopdownloaded)}
              >
                View All Game
              </Button>
            </Box>
          </Grid>
        ) : (
          <Grid>
            {topDownloaded.map((downloaded) => {
              return (
                <Fragment>
                  <Grid
                    key={downloaded.id}
                    item
                    sx={{
                      "& img": {
                        borderRadius: theme.shape.borderRadius,
                        width: "100%",
                      },
                      display: "flex",
                    }}
                  >
                    <Grid item sm={4}>
                      <img src={downloaded.image} alt="DownloadCyborgImage" />
                    </Grid>
                    <Grid
                      item
                      sm={6}
                      sx={{
                        padding: "0px 0px 16px 8px",
                      }}
                    >
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.lighter}
                      >
                        {downloaded.mainName}
                      </Typography>
                      <Typography
                        variant="h6"
                        color={theme.palette.secondary.main}
                      >
                        {downloaded.branchName}
                      </Typography>
                      <Grid
                        container
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Grid item container xs={5} alignItems="center">
                          <GradeIcon
                            sx={{ color: theme.palette.secondary.darker }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {downloaded.voteStar}
                          </Typography>
                        </Grid>
                        <Grid item container xs={6} alignItems="center">
                          <DownloadIcon
                            sx={{ color: theme.palette.primary.lighter }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {downloaded.qualityDownload}
                          </Typography>
                        </Grid>
                        <Grid item sm={1}>
                          <Button
                            sx={{
                              backgroundColor: theme.palette.primary.darker,
                              borderRadius: "50%",
                              minWidth: "0px",
                            }}
                          >
                            <DownloadIcon />
                          </Button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <hr
                    style={{
                      border: `0.5px solid ${theme.palette.secondary.dark}`,
                      margin: "0px 16px 16px 0px",
                    }}
                  />
                </Fragment>
              );
            })}
            <Box textAlign="center">
              <Button
                variant="contained button"
                sx={{
                  bgcolor: theme.palette.primary.lighter,
                  color: theme.palette.secondary.lighter,
                  minHeight: "48.5px",
                  borderRadius: theme.shape.borderRadius,
                  mb: "-20px",
                  "&:hover": {
                    bgcolor: theme.palette.secondary.lighter,
                    color: theme.palette.primary.lighter,
                  },
                  display: viewallTopdownloaded ? "none" : "unset",
                }}
                onClick={() => setViewAllTopDownloaded(!viewallTopdownloaded)}
              >
                View All Game
              </Button>
            </Box>
          </Grid>
        )}
      </Grid>
    </Fragment>
  );
}
