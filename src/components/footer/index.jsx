import React from 'react'
import './index.css'
export default function Footer() {
  return (
    <div
      style={{
        textAlign: 'center',
        marginTop: '30px',
      }}
    >
     <div
      className='content-footer'
     >
        Copyright © 2036 Cyborg Gaming Company. All rights reserved.
     </div>
     <div
      className='content-footer'
     >
        Design: TemplateMo
     </div>
    </div>
  )
}
