import { Button, Grid, Typography } from "@mui/material";
import { Box, useTheme } from "@mui/system";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
export default function TopStreamers() {
  const theme = useTheme();
  const [viewMoreTopStreamer, setViewMoreTopStreamers] = useState(false);
  const { topStreamer } = useSelector((state) => state.topstreamers);
  const { topStreamerviewall } = useSelector(
    (state) => state.topstreamers
  );
  return (
    <Fragment>
      <Grid
        item
        sm={3.5}
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginRight: "16px",
        }}
      >
        <Box
          sx={{
            textAlign: "start",
            padding: "16px 0px 16px 10px",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.light,
              textDecoration: "underline",
            }}
          >
            Top
          </Typography>
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.lighter,
            }}
          >
            Streamers
          </Typography>
        </Box>
        {viewMoreTopStreamer ? (
          <Grid
            container
            justifyContent="space-between"
            sx={{
              overflowY: "scroll",
              height: "450px",
              "::-webkit-scrollbar": {
                width: "5px",
              },
              "::-webkit-scrollbar-track": {
                background: theme.palette.primary.main,
              },
              "::-webkit-scrollbar-thumb ": {
                background: theme.palette.primary.lighter,
              },
              "::-webkit-scrollbar-thumb": {
                "&:hover": {
                  background: theme.palette.secondary.contrastText,
                },
              },
            }}
          >
            {topStreamerviewall.map((streamers, index) => {
              return (
                <Fragment>
                  <Grid
                    key={streamers.id}
                    item
                    sm={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Grid
                      item
                      sm={2}
                      sx={{
                        color: theme.palette.primary.light,
                        fontWeight: 600,
                      }}
                    >
                      {index + 1 > 9 ? index + 1 : `0${index + 1}`}
                    </Grid>
                    <Grid
                      item
                      sm={3}
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: "50%",
                        },
                      }}
                    >
                      <img src={streamers.avatar} alt="Streamers Avatar" />
                    </Grid>
                    <Grid
                      item
                      sm={6}
                      sx={{
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Box>
                        <CheckCircleIcon
                          sx={{ color: theme.palette.primary.lighter }}
                        />
                      </Box>
                      <Typography
                        sx={{
                          color: theme.palette.primary.lighter,
                          marginLeft: "4px",
                        }}
                      >
                        {streamers.mainName}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid>
                    <Button
                      sx={{
                        bgcolor: theme.palette.primary.lighter,
                        color: theme.palette.secondary.lighter,
                        borderRadius: theme.shape.borderRadius,
                        margin: "12px 24px",
                        "&:hover": {
                          bgcolor: theme.palette.secondary.lighter,
                          color: theme.palette.primary.lighter,
                        },
                      }}
                    >
                      Follow
                    </Button>
                  </Grid>
                  <hr
                    style={{
                      border:
                        streamers.mainName === "Areluwa"
                          ? "none"
                          : `0.5px solid ${theme.palette.secondary.dark}`,
                      margin: "0px 16px 16px 0px",
                    }}
                  />
                </Fragment>
              );
            })}
            <Box textAlign="center">
              <Button
                variant="contained button"
                sx={{
                  bgcolor: theme.palette.primary.lighter,
                  color: theme.palette.secondary.lighter,
                  minHeight: "20px",
                  borderRadius: theme.shape.borderRadius,
                  mb: "-20px",
                  "&:hover": {
                    bgcolor: theme.palette.secondary.lighter,
                    color: theme.palette.primary.lighter,
                  },
                  display: viewMoreTopStreamer ? "none" : "unset",
                }}
                onClick={() => setViewMoreTopStreamers(!viewMoreTopStreamer)}
              >
                View All
              </Button>
            </Box>
          </Grid>
        ) : (
          <Grid>
            {topStreamer.map((streamers, index) => {
              return (
                <Fragment>
                  <Grid
                    key={streamers.id}
                    item
                    sm={12}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <Grid
                      item
                      sm={2}
                      sx={{
                        color: theme.palette.primary.light,
                        fontWeight: 600,
                      }}
                    >
                      0{index + 1}
                    </Grid>
                    <Grid
                      item
                      sm={3}
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: "50%",
                        },
                      }}
                    >
                      <img src={streamers.avatar} alt="Streamers Avatar" />
                    </Grid>
                    <Grid
                      item
                      sm={6}
                      sx={{
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Box>
                        <CheckCircleIcon
                          sx={{ color: theme.palette.primary.lighter }}
                        />
                      </Box>
                      <Typography
                        sx={{
                          color: theme.palette.primary.lighter,
                          marginLeft: "4px",
                        }}
                      >
                        {streamers.mainName}
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid>
                    <Button
                      sx={{
                        bgcolor: theme.palette.primary.lighter,
                        color: theme.palette.secondary.lighter,
                        borderRadius: theme.shape.borderRadius,
                        margin: "12px 24px",
                        "&:hover": {
                          bgcolor: theme.palette.secondary.lighter,
                          color: theme.palette.primary.lighter,
                        },
                      }}
                    >
                      Follow
                    </Button>
                  </Grid>
                  <hr
                    style={{
                      border:
                        streamers.mainName === "Areluwa"
                          ? "none"
                          : `0.5px solid ${theme.palette.secondary.dark}`,
                      margin: "0px 16px 16px 0px",
                    }}
                  />
                </Fragment>
              );
            })}
            <Box textAlign="center">
              <Button
                variant="contained button"
                sx={{
                  bgcolor: theme.palette.primary.lighter,
                  color: theme.palette.secondary.lighter,
                  minHeight: "20px",
                  borderRadius: theme.shape.borderRadius,
                  mb: "-20px",
                  "&:hover": {
                    bgcolor: theme.palette.secondary.lighter,
                    color: theme.palette.primary.lighter,
                  },
                  display: viewMoreTopStreamer ? "none" : "unset",
                }}
                onClick={() => setViewMoreTopStreamers(!viewMoreTopStreamer)}
              >
                View All
              </Button>
            </Box>
          </Grid>
        )}
      </Grid>
    </Fragment>
  );
}
