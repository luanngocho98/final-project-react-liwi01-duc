import { Box, Button, Container, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
export default function LiveInStreamPage() {
  const theme = useTheme();
  const [viewAllStreamMostPopular, setViewAllStreamMostPopular] =
    useState(false);
  const { streamsMostPopular } = useSelector(
    (state) => state.streamsmostpulular
  );
  const { streamsMostPopularviewall } = useSelector(
    (state) => state.streamsmostpulular
  );
  return (
    <Fragment>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginTop: "60px",
          position: "relative",
        }}
      >
        <Box
          sx={{
            display: "flex",
            padding: "30px 0px 30px 0px",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.light,
              textDecoration: "underline",
            }}
          >
            Most Popular
          </Typography>
          <Typography
            variant="h2"
            ml={2}
            sx={{
              color: theme.palette.primary.lighter,
            }}
          >
            Live Stream
          </Typography>
        </Box>
        <Box>
          {viewAllStreamMostPopular ? (
            <Grid
              container
              justifyContent="space-between"
              sx={{
                overflowY: "scroll",
                height: "700px",
                "::-webkit-scrollbar": {
                  width: "5px",
                },
                "::-webkit-scrollbar-track": {
                  background: theme.palette.primary.main,
                },
                "::-webkit-scrollbar-thumb ": {
                  background: theme.palette.primary.lighter,
                },
                "::-webkit-scrollbar-thumb": {
                  "&:hover": {
                    background: theme.palette.secondary.contrastText,
                  },
                },
              }}
            >
              {streamsMostPopularviewall.map((livesteam) => {
                return (
                  <Grid
                    key={livesteam.id}
                    item
                    sm={3}
                    p={"16px 8px"}
                    sx={{
                      "& img": {
                        width: "100%",
                        borderRadius: theme.shape.borderRadius,
                      },
                    }}
                  >
                    <Grid
                      item
                      container
                      justifyContent="space-evenly"
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: theme.shape.borderRadius,
                        },
                        ":hover": {
                          cursor: "pointer",
                          "& .viewer": {
                            position: "relative",
                            bottom: "24px",
                            opacity: 1,
                          },
                          "& .live": {
                            position: "relative",
                            top: "40px",
                            opacity: 1,
                          },
                        },
                      }}
                    >
                      <Button
                        className="live"
                        sx={{
                          color: theme.palette.primary.light,
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          padding: "8px 8px",
                          marginTop: "-32px",
                          marginLeft: "60%",
                          display: "flex",
                          justifyContent: "center",
                          opacity: 0,
                        }}
                      >
                        {livesteam.status}
                      </Button>
                      <img src={livesteam.image} alt="LiveStreamImage" />
                      <Button
                        className="viewer"
                        sx={{
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          textAlign: "center",
                          padding: "6px 0px 6px 0px",
                          marginTop: "-16px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          color: theme.palette.primary.light,
                          opacity: 0,
                        }}
                      >
                        <RemoveRedEyeIcon
                          sx={{ fontSize: "13px", paddingRight: "4px" }}
                        />
                        {livesteam.viewer}
                      </Button>
                      <Button
                        className="viewer"
                        sx={{
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          textAlign: "center",
                          marginTop: "-16px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          color: theme.palette.primary.light,
                          opacity: 0,
                        }}
                      >
                        <SportsEsportsIcon
                          sx={{ fontSize: "13px", paddingRight: "4px" }}
                        />
                        {livesteam.games}
                      </Button>
                    </Grid>
                    <Grid container mt={3}>
                      <Grid
                        item
                        sm={3}
                        sx={{
                          "& img": {
                            width: "100%",
                            borderRadius: "50%",
                          },
                        }}
                      >
                        <img src={livesteam.avatar} alt="LiveStream Avatar" />
                      </Grid>
                      <Grid item container sm={8} ml={1}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            color: theme.palette.primary.lighter,
                          }}
                        >
                          <Box
                            sx={{
                              color: theme.palette.primary.lighter,
                            }}
                          >
                            <CheckCircleIcon />
                          </Box>
                          {livesteam.mainName}
                        </Typography>
                        <Typography
                          sx={{
                            color: theme.palette.primary.light,
                            fontWeight: 600,
                          }}
                        >
                          {livesteam.content}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                );
              })}
              <Box textAlign="center">
                <Button
                  variant="contained button"
                  sx={{
                    bgcolor: theme.palette.primary.lighter,
                    color: theme.palette.secondary.lighter,
                    minHeight: "48.5px",
                    borderRadius: theme.shape.borderRadius,
                    mb: "-20px",
                    "&:hover": {
                      bgcolor: theme.palette.secondary.lighter,
                      color: theme.palette.primary.lighter,
                    },
                    display: viewAllStreamMostPopular ? "none" : "unset",
                  }}
                  onClick={() =>
                    setViewAllStreamMostPopular(!viewAllStreamMostPopular)
                  }
                >
                  Load More Streams
                </Button>
              </Box>
            </Grid>
          ) : (
            <Grid container justifyContent="center">
              {streamsMostPopular.map((livesteam) => {
                return (
                  <Grid
                    key={livesteam.id}
                    item
                    sm={3}
                    p={"16px 8px"}
                    sx={{
                      "& img": {
                        width: "100%",
                        borderRadius: theme.shape.borderRadius,
                      },
                    }}
                  >
                    <Grid
                      item
                      container
                      justifyContent="space-evenly"
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: theme.shape.borderRadius,
                        },
                        ":hover": {
                          cursor: "pointer",
                          "& .viewer": {
                            position: "relative",
                            bottom: "24px",
                            opacity: 1,
                          },
                          "& .live": {
                            position: "relative",
                            top: "40px",
                            opacity: 1,
                          },
                        },
                      }}
                    >
                      <Button
                        className="live"
                        item
                        sm={3.5}
                        sx={{
                          color: theme.palette.primary.light,
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          padding: "8px 8px",
                          marginTop: "-32px",
                          marginLeft: "60%",
                          display: "flex",
                          justifyContent: "center",
                          opacity: 0,
                        }}
                      >
                        {livesteam.status}
                      </Button>
                      <img src={livesteam.image} alt="LiveStreamImage" />
                      <Button
                        className="viewer"
                        sm={4.5}
                        sx={{
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          textAlign: "center",
                          padding: "6px 0px 6px 0px",
                          marginTop: "-16px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          color: theme.palette.primary.light,
                          opacity: 0,
                        }}
                      >
                        <RemoveRedEyeIcon
                          sx={{ fontSize: "13px", paddingRight: "4px" }}
                        />
                        {livesteam.viewer}
                      </Button>
                      <Button
                        className="viewer"
                        sm={5.5}
                        sx={{
                          fontSize: "11px",
                          backgroundColor: theme.palette.primary.lighter,
                          borderRadius: "24px",
                          textAlign: "center",
                          marginTop: "-16px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          color: theme.palette.primary.light,
                          opacity: 0,
                        }}
                      >
                        <SportsEsportsIcon
                          sx={{ fontSize: "13px", paddingRight: "4px" }}
                        />
                        {livesteam.games}
                      </Button>
                    </Grid>
                    <Grid container mt={3}>
                      <Grid
                        item
                        sm={3}
                        sx={{
                          "& img": {
                            width: "100%",
                            borderRadius: "50%",
                          },
                        }}
                      >
                        <img src={livesteam.avatar} alt="LiveStreamAvatar" />
                      </Grid>
                      <Grid item container sm={8} ml={1}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            color: theme.palette.primary.lighter,
                          }}
                        >
                          <Box
                            sx={{
                              color: theme.palette.primary.lighter,
                            }}
                          >
                            <CheckCircleIcon />
                          </Box>
                          {livesteam.mainName}
                        </Typography>
                        <Typography
                          sx={{
                            color: theme.palette.primary.light,
                            fontWeight: 600,
                          }}
                        >
                          {livesteam.content}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                );
              })}
              <Box textAlign="center">
                <Button
                  variant="contained button"
                  sx={{
                    bgcolor: theme.palette.primary.lighter,
                    color: theme.palette.secondary.lighter,
                    minHeight: "48.5px",
                    borderRadius: theme.shape.borderRadius,
                    mb: "-20px",
                    "&:hover": {
                      bgcolor: theme.palette.secondary.lighter,
                      color: theme.palette.primary.lighter,
                    },
                    display: viewAllStreamMostPopular ? "none" : "unset",
                  }}
                  onClick={() =>
                    setViewAllStreamMostPopular(!viewAllStreamMostPopular)
                  }
                >
                  Load More Streams
                </Button>
              </Box>
            </Grid>
          )}
        </Box>
      </Container>
    </Fragment>
  );
}
