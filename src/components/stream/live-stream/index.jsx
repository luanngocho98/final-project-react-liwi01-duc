import { Grid, Typography, useTheme } from "@mui/material";
import React, { Fragment } from "react";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";
import { useSelector } from "react-redux";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
export default function LiveStreamDone() {
  const theme = useTheme();
  const { liveStreams } = useSelector((state) => state.live);
  return (
    <Fragment>
      <Grid
        item
        container
        sm={8}
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
        }}
      >
        <Grid
          item
          sm={12}
          sx={{
            display: "flex",
            padding: "16px 0px 0px 10px",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Grid item sx={{ display: "flex" }}>
            <Typography
              variant="h2"
              sx={{
                color: theme.palette.primary.light,
                textDecoration: "underline",
              }}
            >
              Live
            </Typography>
            <Typography
              variant="h2"
              sx={{
                color: theme.palette.primary.lighter,
                marginLeft: "15px",
              }}
            >
              Streams
            </Typography>
          </Grid>
          <Grid item sx={{ color: theme.palette.primary.main }}>
            <KeyboardArrowLeftIcon
              sx={{
                "&:hover": {
                  color: theme.palette.primary.lighter,
                  cursor: "pointer",
                },
              }}
            />
            <KeyboardArrowRightIcon
              sx={{
                "&:hover": {
                  color: theme.palette.primary.lighter,
                  cursor: "pointer",
                },
              }}
            />
          </Grid>
        </Grid>
        <Grid container justifyContent="center" m={"0px -7px"}>
          {liveStreams.map((lives) => {
            return (
              <Grid
                key={lives.id}
                item
                sm={3.85}
                p={"16px 8px"}
                sx={{
                  "& img": {
                    width: "100%",
                    borderRadius: theme.shape.borderRadius,
                  },
                }}
              >
                <Grid
                  item
                  container
                  justifyContent="center"
                  sx={{
                    "& img": {
                      width: "100%",
                      borderRadius: theme.shape.borderRadius,
                    },
                    ":hover": {
                      cursor: "pointer",
                      "& .viewer": {
                        position: "relative",
                        bottom: "32px",
                        opacity: 1,
                      },
                    },
                  }}
                >
                  <img src={lives.image} alt="LiveStreamImage" />
                  <Grid
                    className="viewer"
                    sm={9}
                    sx={{
                      color: theme.palette.primary.lighter,
                      fontWeight: 600,
                      fontSize: "13px",
                      backgroundColor: theme.palette.primary.dark,
                      borderRadius: "24px",
                      textAlign: "center",
                      padding: "10px",
                      marginTop: "-16px",
                      opacity: 0,
                    }}
                  >
                    {lives.viewer}
                  </Grid>
                </Grid>
                <Grid container mt={3} alignItems="center">
                  <Grid item sm={7}>
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {lives.mainName}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    sm={5}
                    alignItems="center"
                    justifyContent="flex-end"
                  >
                    <GradeIcon sx={{ color: theme.palette.secondary.darker }} />
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {lives.voteStar}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container alignItems="center">
                  <Grid item sm={7}>
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.main}
                    >
                      {lives.branchName}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    sm={5}
                    alignItems="center"
                    justifyContent="flex-end"
                  >
                    <DownloadIcon
                      sx={{ color: theme.palette.primary.lighter }}
                    />
                    <Typography
                      variant="h6"
                      color={theme.palette.secondary.lighter}
                    >
                      {lives.qualityDownload}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Fragment>
  );
}
