import { FormControl, FormHelperText, IconButton, MenuItem, Select, useTheme } from '@mui/material'
import React from 'react'
import { Controller } from 'react-hook-form';

export default function Selection(props) {
  const { name, options, control, isRequired } = props;
  const theme = useTheme();
  return (
    <Controller
      name={name}
      control={control}
      rules={{
        required: isRequired ? `Field ${name} này là bắt buộc nhập` : '',
      }}
      defaultValue={options?.find(option => option.isDefault)?.value}
      render={({ field, fieldState }) => {
        return (
          <FormControl
            fullWidth
            sx={{
              '& .MuiSelect-select, .MuiSvgIcon-root': {
                color: theme.palette.secondary.lighter,
              },
              '& .MuiFormHelperText-root': {
                color: 'red',
              },
            }}
          >
            <Select
              {...field}
            >
              {options?.map(option => (
                <MenuItem
                  key={option.value}
                  value={option.value}
                >
                  <IconButton>
                    {option.icon}
                  </IconButton>
                  {option.name}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>
              {fieldState?.error?.message || ''}
            </FormHelperText>
          </FormControl>
        );
      }}
    />
  )
}
