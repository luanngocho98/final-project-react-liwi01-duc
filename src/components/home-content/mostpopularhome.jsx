import React, { Fragment, useEffect, useState } from "react";
import { Button, Container, Grid, Typography, useTheme } from "@mui/material";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";
import { useDispatch, useSelector } from "react-redux";
import { Box } from "@mui/system";
import { GET_MOSTPOPULAR_CARD_HOME } from "../../services/home/mostpopolarhome/constant";
import { getMostpopularCardHome } from "../../services/home/mostpopolarhome/index";
export default function MostPopularCard() {
  const theme = useTheme();
  const dispatch = useDispatch();
  const [popularCardHome, setPopularCardHome] = useState([]);
  const [dataMostPopularAllView, setdataMostPopularAllView] = useState([]);
  const [checkMostPopularView, setCheckMostPopularView] = useState(false);
  const { mostpopularhomecard } = useSelector((state) => state.mostpopolarcard);
  const handleButtonMore = () => {
    setCheckMostPopularView(!checkMostPopularView);
  };
  console.log(mostpopularhomecard);
  useEffect(() => {
    getMostpopularCardHome(
      GET_MOSTPOPULAR_CARD_HOME,
      (success) => {
        const { data } = success;
        dispatch({
          type: GET_MOSTPOPULAR_CARD_HOME,
          payload: data,
        });
        const copyDataMostPopularCard = data?.filter(
          (item, index) => index < 8
        );
        setPopularCardHome(copyDataMostPopularCard);
        const dataMostPopularAll = data;
        setdataMostPopularAllView(dataMostPopularAll);
      },
      (err) => {}
    );
  }, [dispatch]);

  return (
    <Fragment>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginTop: "60px",
          position: "relative",
        }}
      >
        <Box
          sx={{
            display: "flex",
            padding: "30px 0px 30px 0px",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              textDecoration: "underline",
              color: theme.palette.primary.light,
            }}
          >
            Most Popular
          </Typography>
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.lighter,
              ml: 2,
              // 2 === '16px'
            }}
          >
            Right Now
          </Typography>
        </Box>
        <Box>
          {checkMostPopularView ? (
            <Grid
              container
              item
              sx={{
                maxHeight: "600px",
                overflowY: "scroll",
                height: "600px",
                "::-webkit-scrollbar": {
                  width: "5px",
                },
                "::-webkit-scrollbar-track": {
                  background: theme.palette.primary.main,
                },
                "::-webkit-scrollbar-thumb ": {
                  background: theme.palette.primary.lighter,
                },
                "::-webkit-scrollbar-thumb": {
                  "&:hover": {
                    background: theme.palette.secondary.contrastText,
                  },
                },
              }}
            >
              {dataMostPopularAllView.map((cardpopular) => {
                return (
                  // xs={} sm={3} md={} lg={} xl={}
                  <Grid
                    key={cardpopular.id}
                    item
                    sm={2.85}
                    bgcolor={theme.palette.primary.darker}
                    mb={2.5}
                    mr={1.2}
                    p={"24px 18px"}
                    borderRadius="24px"
                    sx={{
                      "& img": {
                        borderRadius: "24px",
                        width: "100%",
                        height: '60%',
                        objectFit: 'cover'
                      },
                    }}
                  >
                    <img alt="CardImage" src={cardpopular.imageUrl} />
                    <Grid container mt={3} alignItems="center">
                      <Grid item sm={7}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.title}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        container
                        sm={5}
                        alignItems="center"
                        justifyContent="flex-end"
                      >
                        <GradeIcon
                          sx={{
                            color: theme.palette.secondary.darker,
                            mr: 0.5,
                          }}
                        />
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.rate}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid container alignItems="center">
                      <Grid item sm={7}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.main}
                        >
                          {cardpopular.subTitle}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        container
                        sm={5}
                        alignItems="center"
                        justifyContent="flex-end"
                      >
                        <DownloadIcon
                          sx={{ color: theme.palette.primary.lighter, mr: 0.5 }}
                        />
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.download}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                );
              })}
            </Grid>
          ) : (
            <Grid item container justifyContent="space-between">
              {popularCardHome.map((cardpopular) => {
                return (
                  // xs={} sm={3} md={} lg={} xl={}
                  <Grid
                    key={cardpopular.id}
                    item
                    sm={2.85}
                    bgcolor={theme.palette.primary.darker}
                    mb={2.5}
                    // mr={1.375}
                    p={"24px 18px"}
                    borderRadius="24px"
                    sx={{
                      "& img": {
                        borderRadius: "24px",
                        width: "100%",
                        height: '60%',
                        objectFit: 'cover'
                      },
                    }}
                  >
                    <img alt="CardImage" src={cardpopular.imageUrl} />
                    <Grid container mt={3} alignItems="center">
                      <Grid item sm={7}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.title}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        container
                        sm={5}
                        alignItems="center"
                        justifyContent="flex-end"
                      >
                        <GradeIcon
                          sx={{
                            color: theme.palette.secondary.darker,
                            mr: 0.5,
                          }}
                        />
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.rate}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid container alignItems="center">
                      <Grid item sm={7}>
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.main}
                        >
                          {cardpopular.subTitle}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        container
                        sm={5}
                        alignItems="center"
                        justifyContent="flex-end"
                      >
                        <DownloadIcon
                          sx={{ color: theme.palette.primary.lighter, mr: 0.5 }}
                        />
                        <Typography
                          variant="h6"
                          color={theme.palette.secondary.lighter}
                        >
                          {cardpopular.download}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                );
              })}
            </Grid>
          )}
          <Box textAlign="center">
            <Button
              variant="contained button"
              sx={{
                bgcolor: theme.palette.primary.lighter,
                color: theme.palette.secondary.lighter,
                minHeight: "48.5px",
                borderRadius: theme.shape.borderRadius,
                mb: "-20px",
                "&:hover": {
                  bgcolor: theme.palette.secondary.lighter,
                  color: theme.palette.primary.lighter,
                },
                display: checkMostPopularView ? "none" : "unset",
              }}
              onClick={handleButtonMore}
            >
              View All
            </Button>
          </Box>
        </Box>
      </Container>
    </Fragment>
  );
}
