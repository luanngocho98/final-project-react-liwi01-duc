import { Box } from "@mui/system";
import React from "react";
import { useNavigate } from "react-router-dom";
import Banner from "../../helper/image/banner-bg.jpg";
import Button from "@mui/material/Button";
import { Typography } from "@mui/material";
import { useTheme } from "@mui/material";
export default function BannerHome() {
  const navigate = useNavigate();
  const theme = useTheme();
  return (
    <Box
      sx={{
        backgroundImage: `url(${Banner})`,
        backgroundPosition: "center center",
        backgroundSize: "cover",
        minHeight: "300px",
        borderRadius: "23px",
        padding: "100px 80px",
      }}
    >
      <Typography
        sx={{
          fontSize: "20px",
          color: "#fff",
          marginBottom: "25px",
        }}
      >
        Welcome To Cyborg
      </Typography>
      <Box
        sx={{
          display: "flex",
        }}
      >
        <Typography
          sx={{
            color: theme.palette.primary.lighter,
            fontWeight: theme.typography.h1.fontWeight,
            fontSize: theme.typography.h1.fontSize,
            textTransform: "uppercase",
            marginTop: "0px",
            marginBottom: "0px",
            marginRight: "20px",
            textDecoration: "underline",
            "&:hover": {
              cursor: "pointer",
            },
          }}
          onClick={() => navigate("/browse")}
        >
          BROWSE
        </Typography>
        <Typography
          sx={{
            fontWeight: theme.typography.h1.fontWeight,
            fontSize: theme.typography.h1.fontSize,
            color: theme.palette.primary.light,
            textTransform: "uppercase",
            marginTop: "0px",
            marginBottom: "0px",
          }}
        >
          OUR
        </Typography>
      </Box>
      <Typography
        sx={{
          fontWeight: theme.typography.h1.fontWeight,
          fontSize: theme.typography.h1.fontSize,
          color: theme.palette.primary.light,
          textTransform: "uppercase",
          marginTop: "0px",
          marginBottom: "0px",
        }}
      >
        POPULAR GAMES HERE
      </Typography>
      <Button
        sx={{
          fontSize: theme.typography.button.fontSize,
          fontWeight: theme.typography.button.fontWeight,
          color: theme.palette.primary.light,
          backgroundColor: theme.palette.primary.lighter,
          borderRadius: theme.shape.borderRadius,
          padding: "12px 30px",
          border: "none",
          marginTop: "24px",
        }}
        onClick={() => navigate("/browse")}
      >
        Browse Now
      </Button>
    </Box>
  );
}
