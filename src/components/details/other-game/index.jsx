import { Box, Button, Container, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";

export default function OtherGames() {
  const theme = useTheme();
  const [viewAllOtherGames, setViewAllOtherGames] = useState(false);
  const { otherGame } = useSelector((state) => state.otherrelatedgame);
  const { otherGameviewall } = useSelector((state) => state.otherrelatedgame);
  return (
    <Fragment>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginTop: "60px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            padding: "30px 0px 30px 0px",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              textDecoration: "underline",
              color: theme.palette.primary.light,
            }}
          >
            Other Related
          </Typography>
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.lighter,
              ml: 2,
              // 2 === '16px'
            }}
          >
            Games
          </Typography>
        </Box>
        <Box>
          {viewAllOtherGames ? (
            <Grid
              container
              justifyContent="space-between"
              sx={{
                overflowY: "scroll",
                height: "600px",
                "::-webkit-scrollbar": {
                  width: "5px",
                },
                "::-webkit-scrollbar-track": {
                  background: theme.palette.primary.main,
                },
                "::-webkit-scrollbar-thumb ": {
                  background: theme.palette.primary.lighter,
                },
                "::-webkit-scrollbar-thumb": {
                  "&:hover": {
                    background: theme.palette.secondary.contrastText,
                  },
                },
              }}
            >
              {otherGameviewall.map((games) => {
                return (
                  <Grid
                    item
                    container
                    sm={6}
                    sx={{
                      marginBottom: "24px",
                    }}
                  >
                    <Grid
                      item
                      sm={3}
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: theme.shape.borderRadius,
                        },
                      }}
                    >
                      <img src={games.image} alt="Games Avatar" />
                    </Grid>
                    <Grid container mt={2} sm={6} sx={{}}>
                      <Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                            ml={1}
                          >
                            {games.mainName}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.main}
                            ml={1}
                          >
                            {games.branchName}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item container sm={3} mt={2}>
                      <Grid>
                        <Grid
                          item
                          container
                          justifyContent="flex-end"
                          sx={{
                            marginRight: "-20px",
                          }}
                        >
                          <GradeIcon
                            sx={{
                              color: theme.palette.secondary.darker,
                              mr: 0.5,
                            }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {games.voteStar}
                          </Typography>
                        </Grid>
                        <Grid item container justifyContent="flex-end">
                          <DownloadIcon
                            sx={{
                              color: theme.palette.primary.lighter,
                              mr: 0.5,
                            }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {games.qualityDownload}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <hr
                      style={{
                        textAlign: "center",
                        width: "100%",
                        border: `0.5px solid ${theme.palette.secondary.dark}`,
                        margin: "24px 24px 8px 0px",
                      }}
                    />
                  </Grid>
                );
              })}
              <Box textAlign="center">
                <Button
                  variant="contained button"
                  sx={{
                    bgcolor: theme.palette.primary.lighter,
                    color: theme.palette.secondary.lighter,
                    minHeight: "48.5px",
                    borderRadius: theme.shape.borderRadius,
                    mb: "-20px",
                    "&:hover": {
                      bgcolor: theme.palette.secondary.lighter,
                      color: theme.palette.primary.lighter,
                    },
                    display: viewAllOtherGames ? "none" : "unset",
                  }}
                  onClick={() => setViewAllOtherGames(!viewAllOtherGames)}
                >
                  View All Game
                </Button>
              </Box>
            </Grid>
          ) : (
            <Grid
              sx={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "center",
              }}
            >
              {otherGame.map((games) => {
                return (
                  <Grid
                    item
                    container
                    sm={6}
                    sx={{
                      marginBottom: "24px",
                    }}
                  >
                    <Grid
                      item
                      sm={3}
                      sx={{
                        "& img": {
                          width: "100%",
                          borderRadius: theme.shape.borderRadius,
                        },
                      }}
                    >
                      <img src={games.image} alt="Games Avatar" />
                    </Grid>
                    <Grid container mt={2} sm={6} sx={{}}>
                      <Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                            ml={1}
                          >
                            {games.mainName}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.main}
                            ml={1}
                          >
                            {games.branchName}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item container sm={3} mt={2}>
                      <Grid>
                        <Grid
                          item
                          container
                          justifyContent="flex-end"
                          sx={{
                            marginRight: "-20px",
                          }}
                        >
                          <GradeIcon
                            sx={{
                              color: theme.palette.secondary.darker,
                              mr: 0.5,
                            }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {games.voteStar}
                          </Typography>
                        </Grid>
                        <Grid item container justifyContent="flex-end">
                          <DownloadIcon
                            sx={{
                              color: theme.palette.primary.lighter,
                              mr: 0.5,
                            }}
                          />
                          <Typography
                            variant="h6"
                            color={theme.palette.secondary.lighter}
                          >
                            {games.qualityDownload}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                    <hr
                      style={{
                        textAlign: "center",
                        width: "100%",
                        border: `0.5px solid ${theme.palette.secondary.dark}`,
                        margin: "24px 24px 8px 0px",
                      }}
                    />
                  </Grid>
                );
              })}
              <Box textAlign="center">
                <Button
                  variant="contained button"
                  sx={{
                    bgcolor: theme.palette.primary.lighter,
                    color: theme.palette.secondary.lighter,
                    minHeight: "48.5px",
                    borderRadius: theme.shape.borderRadius,
                    mb: "-20px",
                    "&:hover": {
                      bgcolor: theme.palette.secondary.lighter,
                      color: theme.palette.primary.lighter,
                    },
                    display: viewAllOtherGames ? "none" : "unset",
                  }}
                  onClick={() => setViewAllOtherGames(!viewAllOtherGames)}
                >
                  View All Game
                </Button>
              </Box>
            </Grid>
          )}
        </Box>
      </Container>
    </Fragment>
  );
}
