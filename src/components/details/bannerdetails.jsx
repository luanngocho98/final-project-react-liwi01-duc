import React, { Fragment } from "react";
import featureleft from "../../helper/image/feature-left.jpg";
import featureright from "../../helper/image/feature-right.jpg";
import ArrowRightSharpIcon from "@mui/icons-material/ArrowRightSharp";
import { Box, Grid, Typography, useTheme } from "@mui/material";
export default function BannerDetails() {
  const theme = useTheme();
  return (
    <Fragment>
      <Grid item container>
        <Grid
          item
          container
          sm={3.5}
          sx={{
            "& img": {
              width: "100%",
              borderRadius: theme.shape.borderRadius,
            },
            marginBottom: "40px",
          }}
        >
          <img src={featureleft} alt="Feature Avatar" />
        </Grid>
        <Grid
          item
          sm={8}
          sx={{
            "& img": {
              width: "100%",
              borderRadius: theme.shape.borderRadius,
              marginLeft: "24px",
            },
          }}
        >
          <img src={featureright} alt="Feature Avatar" />
          <Grid
            item
            container
            alignItems="center"
            sx={{
              "& a": {
                // transform: translate(-23px, -23px),
                width: "46px",
                height: "46px",
                backgroundColor: "#fff",
                borderRadius: "50%",
                color: "#ec6090",
                marginTop: "-50%",
                marginLeft: "50%",
              },
            }}
          >
            <a
              href="https://www.youtube.com/watch?v=r1b03uKWk_M"
              target="_blank"
              rel="noreferrer"
            >
              <ArrowRightSharpIcon
                sx={{
                  fontSize: "48px",
                }}
              />
            </a>
          </Grid>
        </Grid>
      </Grid>
      <Box>
        <Typography
          variant='h1'
          sx={{
            color: theme.palette.primary.light,
            textAlign: 'center',
            margin: '32px 0px 32px 0px'
          }}
        >
          FORTNITE DETAILS
        </Typography>
      </Box>
    </Fragment>
  );
}
