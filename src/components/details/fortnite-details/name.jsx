import { Box, Grid, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import GradeIcon from "@mui/icons-material/Grade";
import DownloadIcon from "@mui/icons-material/Download";
import SummarizeIcon from '@mui/icons-material/Summarize';
import SportsEsportsIcon from '@mui/icons-material/SportsEsports';
export default function FortNiteDetails() {
  const theme = useTheme();
  const { fortnite } = useSelector((state) => state.fortnitedetails);
  return (
    <Grid>
      {fortnite.map((fortnitedetail) => {
        return (
          <Grid
            sx={{
              display: 'flex',
              padding: '24px 0px 24px 0px',
              justifyContent: 'space-between'
            }}
          
          >
            <Grid
              item
              container
              sm={6}
              sx={{
                borderRadius: theme.shape.borderRadius,
                backgroundColor: theme.palette.primary.darker,
                padding: '8px 16px',
              }}
            >
              <Grid container mt={2}>
                <Grid item sm={6}>
                  <Typography
                    variant="h6"
                    color={theme.palette.secondary.lighter}
                    ml={1}
                  >
                    {fortnitedetail.mainName}
                  </Typography>
                </Grid>
                <Grid
                  item
                  container
                  sm={5}
                  justifyContent="flex-end"
                >
                  <GradeIcon sx={{ color: theme.palette.secondary.darker, mr: 0.5 }} />
                  <Typography
                    variant="h6"
                    color={theme.palette.secondary.lighter}
                    mr={1}
                  >
                    {fortnitedetail.voteStar}
                  </Typography>
                </Grid>
              </Grid>
              <Grid item container mb={2}>
                <Grid item sm={6}>
                  <Typography variant="h6" color={theme.palette.secondary.main} ml={1}>
                    {fortnitedetail.branchName}
                  </Typography>
                </Grid>
                <Grid
                  item
                  container
                  sm={5}
                  justifyContent="flex-end"
                >
                  <DownloadIcon
                    sx={{ color: theme.palette.primary.lighter, mr: 0.5 }}
                  />
                  <Typography
                    variant="h6"
                    color={theme.palette.secondary.lighter}
                    mr={1}
                  >
                    {fortnitedetail.qualityDownload}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              container
              sm={5.5}
              sx={{
                borderRadius: theme.shape.borderRadius,
                backgroundColor: theme.palette.primary.darker,
                padding: '24px 24px 24px 24px',
                textAlign: 'center'
              }}
            >
              <Grid
                sm={3}
              >
                <Box>
                  <GradeIcon sx={{color: theme.palette.secondary.darker}}/>
                </Box>
                <Typography sx={{color: theme.palette.primary.light}}>
                  {fortnitedetail.voteStar}
                </Typography>
              </Grid>
              <Grid
                sm={3}
              >
                <Box>
                  <DownloadIcon sx={{color: theme.palette.primary.lighter}}/>
                </Box>
                <Typography sx={{color: theme.palette.primary.light}}>
                  {fortnitedetail.qualityDownload}
                </Typography>
              </Grid>
              <Grid
                sm={3}
              >
                <Box>
                  <SummarizeIcon sx={{color: theme.palette.primary.lighter}}/>
                </Box>
                <Typography sx={{color: theme.palette.primary.light}}>
                  {fortnitedetail.gigabyte}
                </Typography>
              </Grid>
              <Grid
                sm={3}
              >
                <Box>
                  <SportsEsportsIcon sx={{color: theme.palette.primary.lighter}}/>
                </Box>
                <Typography sx={{color: theme.palette.primary.light}}>
                  {fortnitedetail.methods}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </Grid>
  );
}
