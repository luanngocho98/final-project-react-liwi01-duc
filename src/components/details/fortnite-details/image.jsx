import { Button, Grid, Typography } from '@mui/material'
import { Box, useTheme } from '@mui/system'
import React, { Fragment } from 'react'
import { useSelector } from 'react-redux'

export default function ImageAndContent() {
  const theme = useTheme()
  const { imageoffort } = useSelector(state => state.fortnitedetails)
  return (
    <Grid container justifyContent="space-between">
      {imageoffort.map(imagefort => {
        return(
          <Fragment>
            <Grid
            key={imagefort.id}
            sm={3.75}
            p={'16px 0px'}
            sx={{
              '& img': {
                width: '100%',
                borderRadius: theme.shape.borderRadius
              }
            }}
            >
              <img src={imagefort.image} alt='FortNiteImage'/>
            </Grid>
          </Fragment>
        )
      })}
      <Box>
        <Typography
          sx={{
            color: theme.palette.primary.main,
            margin: '24px 0px'
          }}
        >
        Cyborg Gaming is free HTML CSS website template provided by TemplateMo. This is Bootstrap v5.2.0 layout. You can make a small contribution via PayPal to info [at] templatemo.com and thank you for supporting. If you want to get the PSD source files, please contact us. Lorem ipsum dolor sit consectetur es dispic dipiscingei elit, sed doers eiusmod lisum hored tempor.
        </Typography>
      </Box>
      <Grid 
        sm={12}
        sx={{
          textAlign: 'center',
          justifyContent: 'space-between',
          marginBottom: '36px'
        }}
      >
        <Button
          sx={{
            border: `1px solid ${theme.palette.primary.lighter} `,
            color: theme.palette.primary.lighter,
            padding: "8px 20px",
            borderRadius: theme.shape.borderRadius,
            marginTop: '24px',
            width: '100%'
          }}
        >
          Download Fortnite Now
        </Button>
      </Grid>
    </Grid>
  )
}
