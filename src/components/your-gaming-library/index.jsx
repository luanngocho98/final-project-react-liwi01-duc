/* eslint-disable jsx-a11y/img-redundant-alt */
import {
  Box,
  Button,
  Container,
  Grid,
  Typography,
  useTheme,
} from "@mui/material";
import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { getYourGamingLibrary } from '../../services/yourgamingLibrary/index'
import {GET_YOURGAMING_LIBRARY } from '../../services/yourgamingLibrary/constant'
export default function YourGamingLibrary() {
  const navigate = useNavigate();
  const theme = useTheme();
  const [checkYourlibraryatHome, setcheckYourlibraryAtHome] = useState(false);
  const { yourGamingLibrary } = useSelector((state) => state.gaminglibrary);
  const { yourGamingLibraryviewall } = useSelector(
    (state) => state.gaminglibrary
  );
  const dispatch = useDispatch();
  const [limitedYourGamingLibrary, setLimitedYourGamingLibrary] = useState([]);
  const [allYourGamingLibrary, setAllYourGamingLibrary] = useState([])
  const [isButtonCheckViewAll, setIsButtonCheckViewAll] = useState(false)
  const [isDisable, setIsDisable] = useState(false)
  console.log(isDisable,'---isDisable')
  const PrimaryContent = styled("div")({
    color: theme.palette.primary.light,
    fontWeight: 700,
    fontSize: "15px",
    mb: "5px",
  });

  const SecondContent = styled("div")({
    color: theme.palette.primary.main,
    fontSize: "15px",
  });

  useEffect(() => {
    getYourGamingLibrary(
      GET_YOURGAMING_LIBRARY,
      (success) => {
        const { data } = success;
        dispatch({
          type: GET_YOURGAMING_LIBRARY,
          payload: data
        });
        const copyYourGamingLibrary = data?.filter(
          (item, index) => index < 3
        );
        const dataYourGamingLibrary = data
        setLimitedYourGamingLibrary(copyYourGamingLibrary)
        setAllYourGamingLibrary(dataYourGamingLibrary)
      },
      (err) => {}

    )
  },[dispatch])
  return (
    <Fragment>
      <Container
        maxWidth="350px"
        sx={{
          backgroundColor: theme.palette.primary.dark,
          borderRadius: theme.shape.borderRadius,
          marginTop: "60px",
          position: "relative",
          paddingTop: "30px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            paddingBottom: "30px",
          }}
        >
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.light,
              textDecoration: "underline",
            }}
          >
            Your Gaming
          </Typography>
          <Typography
            variant="h2"
            sx={{
              color: theme.palette.primary.lighter,
              ml: 2,
            }}
          >
            Library
          </Typography>
        </Box>
        <Box>
          {checkYourlibraryatHome ? (
            <Grid
              item
              sx={{
                overflowY: "scroll",
                height: "600px",
                "::-webkit-scrollbar": {
                  width: "5px",
                },
                "::-webkit-scrollbar-track": {
                  background: theme.palette.primary.main,
                },
                "::-webkit-scrollbar-thumb ": {
                  background: theme.palette.primary.lighter,
                },
                "::-webkit-scrollbar-thumb": {
                  "&:hover": {
                    background: theme.palette.secondary.contrastText,
                  },
                },
              }}
            >
              {allYourGamingLibrary.map((keyId) => {
                return (
                  <Box
                    key={keyId.id}
                    sx={{
                      display: "flex",
                      borderBottom: `1px solid ${theme.palette.primary.darker}`,
                      marginBottom: "20px",
                      paddingBottom: "20px",
                      justifyContent: "space-between",
                      alignItems: "center",
                      "& img": {
                        maxWidth: "100px",
                        minHeight: '80px',
                        borderRadius: "23px",
                        objectFit: 'cover'
                      },
                    }}
                  >
                    <Box>
                      <img src={keyId.imageUrl} alt="Library Avatar" />
                    </Box>
                    <Box>
                      <PrimaryContent>{keyId.gameTitle}</PrimaryContent>
                      <SecondContent>{keyId.device}</SecondContent>
                    </Box>
                    <Box>
                      <PrimaryContent>Date Added</PrimaryContent>
                      <SecondContent>{keyId.dateAdded}</SecondContent>
                    </Box>
                    <Box>
                      <PrimaryContent>Hours Played</PrimaryContent>
                      <SecondContent>{keyId.hoursPlayed}</SecondContent>
                    </Box>
                    <Box>
                      <PrimaryContent>Currently</PrimaryContent>
                      <SecondContent>{keyId.downloaded ? "Download" : "Downloaded"}</SecondContent>
                    </Box>
                    <Button
                      onClick={() => navigate("/login")}
                      sx={{
                        border: keyId.downloaded
                          ? `0.5px solid ${theme.palette.primary.lighter}`
                          : `0.5px solid ${theme.palette.primary.light}`,
                        fontSize: theme.typography.button.fontSize,
                        borderRadius: theme.shape.borderRadius,
                        padding: keyId.downloaded ? "12px 30px" : '12px 21px'
                      }}
                    >
                      {keyId.downloaded ? "Download" : "Downloaded"}
                    </Button>
                  </Box>
                );
              })}
            </Grid>
          ) : (
            limitedYourGamingLibrary.map((keyId) => {
              return (
                <Box
                  key={keyId.id}
                  sx={{
                    display: "flex",
                    borderBottom: `1px solid ${theme.palette.primary.darker}`,
                    marginBottom: "20px",
                    paddingBottom: "20px",
                    justifyContent: "space-between",
                    alignItems: "center",
                    "& img": {
                      maxWidth: "100px",
                      minHeight: '80px',
                      borderRadius: "23px",
                      objectFit: 'cover'
                    },
                  }}
                >
                  <Box>
                    <img src={keyId.imageUrl} alt="Library Avatar" />
                  </Box>
                  <Box>
                    <PrimaryContent>{keyId.gameTitle}</PrimaryContent>
                    <SecondContent>{keyId.device}</SecondContent>
                  </Box>
                  <Box>
                    <PrimaryContent>Date Added</PrimaryContent>
                    <SecondContent>{keyId.dateAdded}</SecondContent>
                  </Box>
                  <Box>
                    <PrimaryContent>Hours Played</PrimaryContent>
                    <SecondContent>{keyId.hoursPlayed}</SecondContent>
                  </Box>
                  <Box>
                    <PrimaryContent>Currently</PrimaryContent>
                    <SecondContent>{keyId.downloaded ? "Download" : "Downloaded"}</SecondContent>
                  </Box>
                  <Button
                    onClick={() => navigate("/login")}
                    disabled={isDisable}
                    sx={{
                      border: keyId.downloaded
                        ? `0.5px solid ${theme.palette.primary.lighter}`
                        : `0.5px solid ${theme.palette.primary.light}`,
                      fontSize: theme.typography.button.fontSize,
                      borderRadius: theme.shape.borderRadius,
                      padding: keyId.downloaded ? "12px 30px" : '12px 21px',
                    }}
                  >
                    {keyId.downloaded ? "Download" : "Downloaded"}
                  </Button>
                </Box>
              );
            })
          )}
          <Box textAlign="center">
            <Button
              variant="contained button"
              sx={{
                bgcolor: theme.palette.primary.lighter,
                color: theme.palette.secondary.lighter,
                minHeight: "48.5px",
                borderRadius: theme.shape.borderRadius,
                mb: "-20px",
                "&:hover": {
                  bgcolor: theme.palette.secondary.lighter,
                  color: theme.palette.primary.lighter,
                },
                display: checkYourlibraryatHome ? "none" : "unset",
              }}
              onClick={() => setcheckYourlibraryAtHome(!checkYourlibraryatHome)}
            >
              View All
            </Button>
          </Box>
        </Box>
      </Container>
    </Fragment>
  );
}
