import React, { Fragment } from "react";
import logologin from "../../helper/image/logo.png";
import ProfileHeader from "../../helper/image/profile-header.jpg";
import SearchIcon from "@mui/icons-material/Search";
import { Link, useLocation, useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  InputAdornment,
  TextField,
  useTheme,
} from "@mui/material";
import { useSelector } from "react-redux";
import LoginButton from '../../pages/login/index'

export default function Header() {
  const { isAuthen } = useSelector((state) => state.authen);
  const location = useLocation()
  const theme = useTheme();
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        mx: "150px",
        "& a": {
          color: "#666",
          fontSize: "14px",
          textDecoration: "none",
          ":hover": {
            color: "#e75e8d",
          },
        },
        "& img": {
          ":hover": {
            cursor: 'pointer'
          }
        }
      }}
    >
      <Box
        sx={{
          borderRight: `0.5px solid ${theme.palette.secondary.dark}`,
        }}
      >
        <img
          className="main-logo"
          src={logologin}
          onClick={() => navigate("/")}
          alt="Cyborg Logo"
        />
      </Box>
      <TextField
        placeholder="Type Something"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon sx={{ color: "white", zIndex: 1 }} />
            </InputAdornment>
          ),
        }}
        sx={{
          "& .MuiInputBase-root": {
            borderRadius: "32px",
            backgroundColor: "#27292a",
            fontSize: "14px",
          },
          "& .MuiInputBase-root:hover": {
            border: `0.5px solid ${theme.palette.secondary.contrastText}`,
          },
          "& input": {
            color: "white",
          },
        }}
      />
      <Link to="/" 
        style={{
          color: location.pathname === '/' ? theme.palette.primary.lighter : theme.palette.primary.main
        }}
      >
        Home
      </Link>
      <Link to="browse" 
        style={{
          color: location.pathname === '/browse' ? theme.palette.primary.lighter : theme.palette.primary.main
        }}
      >
        Browse
      </Link>
      <Link to="details"
        style={{
          color: location.pathname === '/details' ? theme.palette.primary.lighter : theme.palette.primary.main
        }}
      >
        Details
      </Link>
      <Link to="streams"
        style={{
          color: location.pathname === '/streams' ? theme.palette.primary.lighter : theme.palette.primary.main
        }}
      >
        Streams
      </Link>
      {
        !isAuthen && (
          <LoginButton/>
        )
      }
      {
        isAuthen && (
          <Button
            sx={{
              "& img": {
                borderRadius: "50%",
                ml: 1.5,
              },
              textTransform: "unset",
              color: theme.palette.secondary.lighter,
              fontSize: "14px",
              fontWeight: "unset",
              borderRadius: "23px",
              p: "6px 12px",
              bgcolor: theme.palette.primary.lighter,
            }}
            onClick={() => navigate("/profile")}
          >
            Profile
            <img src={ProfileHeader} alt="Profile Avatar" />
          </Button>
        )
      }
    </Box>
  );
}
