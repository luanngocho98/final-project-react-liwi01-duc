import React, { useState } from 'react'
import { Route, Routes } from 'react-router-dom'
// layouts
import DefaultLayout from '../layouts/DefaultLayout'
import HomePage from '../pages/home'
// pages
import LoginPage from '../pages/login'
import RegisterPage from '../pages/register'
import Profile from '../pages/profile'
import Browse from '../pages/browse'
import Details from '../pages/details'
import Streams from '../pages/streams'
// path for app
import { path } from './path'
import GameCreate from '../pages/game/create'

export default function Router() {
  // get from store
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />} >
        <Route path={path.home} element={<HomePage />} />
        <Route path={path.login} element={<LoginPage onClick={handleClickOpen} open={open} setOpen={setOpen}/>} />
        <Route path={path.register} element={<RegisterPage />} />
        <Route path={path.profile} element={<Profile/>} />
        <Route path={path.browse} element={<Browse />} />
        <Route path={path.details} element={<Details />} />
        <Route path={path.streams} element={<Streams />} />
        <Route path={path.gameCreate} element={<GameCreate />} />
      </Route>
    </Routes>
  )
}

// Không được đứng trực tiếp ở main để thao tác git add or commit or push

// *** muốn biết đang đứng ở branch nào : gõ lệnh : git branch (nhấn chữ q để end git branch)

// 0. create file .gitignore và paste git ignore trên gitlab vào

// 1. Kiểm tra tất cả code đã được save
// 2. Sử dụng lệnh : git checkout -b branch_name ( vd: git checkout -b feat/init-project )
// 3. Sử dụng lệnh : git add . ( để add tất cả các file lên trạng thái staged )
// 4. Sử dụng lệnh : git commit -m 'commit_name' ( vd: git commit -m 'Init project, install libraries support, set up router' )
// 5. Sử dụng lệnh : git push --set-upstream origin branch_name ( để đẩy code lên repository )
// Hello
//xinchao