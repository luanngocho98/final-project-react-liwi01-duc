export const path = {
  home: '/',
  login: '/login',
  register: '/register',
  profile: '/profile',
  browse: '/browse',
  details: '/details',
  streams: '/streams',
  gameCreate: '/admin/game-create',
}
