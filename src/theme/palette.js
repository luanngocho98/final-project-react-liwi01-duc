export const palette = {
  primary: {
    lighter: '#e75e8d',
    light: '#fff',
    main: '#666',
    dark: '#1f2122',
    darker: '#27292a',
    contrastText: '#223250',
  },
  secondary: {
    lighter: '#FFFFFF',
    light: '#fff',
    main: '#666',
    dark: '#27292a',
    darker: '#ffff00',
    contrastText: '#DBA39A',
  },
  // success: {},
  // error: {},
  // grey: {},
}
