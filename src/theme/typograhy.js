export const typography = {
  h1: {
    fontSize: '45px',
    fontWeight: 700,
  },
  h2: {
    fontSize: '34px',
    fontWeight: 700,
  },
  // h3: {},
  h4: {
    fontSize: "34px",
    fontWeight: 700,
  },
  // h5: {},
  h6: {
   fontSize: '13px',
   fontWeight: 500
  },
  button: {
    fontSize: '14px',
    fontWeight: 500,
  }
  // body1: {},
  // body2: {},
}
