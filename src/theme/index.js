import { createTheme } from "@mui/material";
import { palette } from "./palette";
import { shape } from "./shape";
import { typography } from "./typograhy";

export const theme = createTheme({
  palette: palette,
  typography: typography,
  shape: shape,
  // shadows: {},
  // spacing: {},
});
