import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../components/header'
import Footer from '../components/footer'
import { Box, useTheme } from '@mui/material'
import AuthenticationDialog from '../components/Dialog/Login'
import SnackbarCustom from '../components/Snackbar'

export default function DefaultLayout() {
  const theme = useTheme();
  return (
    <div>
      <Box
        sx={{
          position: 'fixed',
          top: '0px',
          left: '0px',
          right: '0px',
          zIndex: 3,
          backgroundColor: theme.palette.primary.dark,
          padding: '24px'
        }}
      >
        <Header/>
      </Box>
      <Box
        margin="24px 150px"
        padding="64px"
        bgcolor={theme.palette.primary.darker}
        borderRadius="24px"
      >
        <Outlet />
      </Box>
      <Footer />
      <AuthenticationDialog />
      <SnackbarCustom />
    </div>
  )
}
