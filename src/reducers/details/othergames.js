import Game1 from "../../helper/image/game-01.jpg";
import Game2 from "../../helper/image/game-02.jpg";
import Game3 from "../../helper/image/game-03.jpg";

const initialState = {
  otherGame: [
    {
      image: Game1,
      mainName: "Dota 2",
      branchName: 'SandBox',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 1,
    },
    {
      image: Game2,
      mainName: "PubG",
      branchName: 'Battle S',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 2,
    },
    {
      image: Game3,
      mainName: "Dota2",
      branchName: 'Steam-X',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 3,
    },
    {
      image: Game1,
      mainName: "CS-GO",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 4,
    },
    {
      image: Game2,
      mainName: "Warface",
      branchName: 'Max 3D',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 5,
    },
    {
      image: Game3,
      mainName: "Mini Craft",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 6,
    },
  ],
  otherGameviewall: [
    {
      image: Game1,
      mainName: "Dota 2",
      branchName: 'SandBox',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 1,
    },
    {
      image: Game2,
      mainName: "PubG",
      branchName: 'Battle S',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 2,
    },
    {
      image: Game3,
      mainName: "Dota2",
      branchName: 'Steam-X',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 3,
    },
    {
      image: Game1,
      mainName: "CS-GO",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 4,
    },
    {
      image: Game2,
      mainName: "Warface",
      branchName: 'Max 3D',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 5,
    },
    {
      image: Game3,
      mainName: "Mini Craft",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 6,
    },
    {
      image: Game1,
      mainName: "CS-GO",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 7,
    },
    {
      image: Game2,
      mainName: "Warface",
      branchName: 'Max 3D',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 8,
    },
    {
      image: Game3,
      mainName: "Mini Craft",
      branchName: 'Legend',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      id: 9,
    },
  ],
};

const OtherRelatedGames = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default OtherRelatedGames;
