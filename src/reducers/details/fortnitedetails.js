import details1 from '../../helper/image/details-01.jpg';
import details2 from '../../helper/image/details-02.jpg';
import details3 from '../../helper/image/details-03.jpg';

const initialState = {
	fortnite: [
		{
			mainName: "FortNite",
			branchName: "Sandbox",
			voteStar: '4.8',
			qualityDownload: '2.3M',
			gigabyte: '36GB',
			methods: 'Action'
		},
	],
  imageoffort: [
    {
      id: 1,
			image: details1
    },
		{
      id: 2,
			image: details2
    },
		{
      id: 3,
			image: details3
    },
  ]
}

const FortNite = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
} 

export default FortNite;