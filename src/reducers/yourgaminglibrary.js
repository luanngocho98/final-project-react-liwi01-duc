import Game1 from '../helper/image/game-01.jpg'
import Game2 from '../helper/image/game-02.jpg'
import Game3 from '../helper/image/game-03.jpg'
import { GET_YOURGAMING_LIBRARY } from '../services/yourgamingLibrary/constant'
const initialState = {
  yourGamingLibrary : []
}

const YourGamingLibrary = (state = initialState, action) => {
  switch(action.type) {
    case GET_YOURGAMING_LIBRARY: {
      return {
        ...state,
        yourGamingLibrary: action.payload.data
      }
    }
    default:
      return state;
  }
}

export default YourGamingLibrary;