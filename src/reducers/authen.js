const initialState = {
  isAuthen: false,
}
const authenReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
export default authenReducer;