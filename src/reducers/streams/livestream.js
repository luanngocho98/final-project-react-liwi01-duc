import Featured01 from '../../helper/image/featured-01.jpg'
import Featured02 from '../../helper/image/featured-02.jpg'
import Featured03 from '../../helper/image/featured-03.jpg'

const initialState = {
  liveStreams: [
    {
      id: 1,
      image: Featured01,
      mainName: 'CS-GO',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
    {
      id: 2,
      image: Featured02,
      mainName: 'Gamezer',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
    {
      id: 3,
      image: Featured03,
      mainName: 'Island Rusty',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
  ]
}

const LiveStreams = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
} 

export default LiveStreams;