import stream1 from "../../helper/image/stream-01.jpg";
import stream2 from "../../helper/image/stream-02.jpg";
import stream3 from "../../helper/image/stream-03.jpg";
import stream4 from "../../helper/image/stream-04.jpg";
import avatar1 from "../../helper/image/avatar-01.jpg";
import avatar2 from "../../helper/image/avatar-02.jpg";
import avatar3 from "../../helper/image/avatar-03.jpg";
import avatar4 from "../../helper/image/avatar-04.jpg";
const initialState = {
  streamsMostPopular: [
    {
      id: 1,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 2,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 3,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 4,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
		{
      id: 5,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 6,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 7,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 8,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
  ],
  streamsMostPopularviewall: [
    {
      id: 1,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 2,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 3,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 4,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 5,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 6,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 7,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 8,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 9,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 19,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 54,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 34,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 76,
      image: stream1,
      avatar: avatar1,
      mainName: "KenganC",
      content: "Just Talking With Fans",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 12,
      image: stream2,
      avatar: avatar2,
      mainName: "LunaMa",
      content: "CS-GO 36 Hours Live Stream",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 71,
      image: stream3,
      avatar: avatar3,
      mainName: "Areluwa",
      content: "Maybe Nathej Allnight Chillin",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 18,
      image: stream4,
      avatar: avatar4,
      mainName: "GangTm",
      content: "Live Streaming Till Morning",
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
  ],
};

const StreamsMostPopular = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
export default StreamsMostPopular;
