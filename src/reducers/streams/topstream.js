import avatar1 from "../../helper/image/avatar-01.jpg";
import avatar2 from "../../helper/image/avatar-02.jpg";
import avatar3 from"../../helper/image/avatar-03.jpg";
const initialState = {
  topStreamer : [
    {
      id: 1,
      avatar: avatar1,
      mainName: 'KenganC',
    },
    {
      id: 2,
      avatar: avatar2,
      mainName: 'LunaMa',
    },
    {
      id: 3,
      avatar: avatar3,
      mainName: 'Areluwa',
    },
  ],
  topStreamerviewall: [
    {
      id: 1,
      avatar: avatar1,
      mainName: "KenganC",
    },
    {
      id: 2,
      avatar: avatar2,
      mainName: "LunaMa",
    },
    {
      id: 3,
      avatar: avatar3,
      mainName: "Areluwa",
    },
    {
      id: 4,
      avatar: avatar1,
      mainName: "KenganC",
    },
    {
      id: 5,
      avatar: avatar2,
      mainName: "LunaMa",
    },
    {
      id: 6,
      avatar: avatar3,
      mainName: "Areluwa",
    },
  ],
}

const TopStreamers = (state = initialState, action) => {
  switch(action.type) {
    default: 
    return state;
  }
}
export default TopStreamers;