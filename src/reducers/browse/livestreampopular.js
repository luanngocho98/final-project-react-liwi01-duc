import stream1 from '../../helper/image/stream-01.jpg';
import stream2 from '../../helper/image/stream-02.jpg';
import stream3 from '../../helper/image/stream-03.jpg';
import stream4 from '../../helper/image/stream-04.jpg';
import avatar1 from '../../helper/image/avatar-01.jpg';
import avatar2 from '../../helper/image/avatar-02.jpg';
import avatar3 from '../../helper/image/avatar-03.jpg';
import avatar4 from '../../helper/image/avatar-04.jpg';
import service1 from '../../helper/image/service-01.jpg';
import service2 from '../../helper/image/service-02.jpg';
import service3 from '../../helper/image/service-03.jpg';
const initialState = {
  mostpopularLiveStream : [
    {
      id: 1,
      image: stream1,
      avatar: avatar1,
      mainName: 'KenganC',
      content: 'Just Talking With Fans',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 2,
      image: stream2,
      avatar: avatar2,
      mainName: 'LunaMa',
      content: 'CS-GO 36 Hours Live Stream',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 3,
      image: stream3,
      avatar: avatar3,
      mainName: 'Areluwa',
      content: 'Maybe Nathej Allnight Chillin',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 4,
      image: stream4,
      avatar: avatar4,
      mainName: 'GangTm',
      content: 'Live Streaming Till Morning',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
  ],
  mostpopularLiveStreamviewmore : [
    {
      id: 1,
      image: stream1,
      avatar: avatar1,
      mainName: 'KenganC',
      content: 'Just Talking With Fans',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 2,
      image: stream2,
      avatar: avatar2,
      mainName: 'LunaMa',
      content: 'CS-GO 36 Hours Live Stream',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 3,
      image: stream3,
      avatar: avatar3,
      mainName: 'Areluwa',
      content: 'Maybe Nathej Allnight Chillin',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 4,
      image: stream4,
      avatar: avatar4,
      mainName: 'GangTm',
      content: 'Live Streaming Till Morning',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 5,
      image: stream1,
      avatar: avatar1,
      mainName: 'KenganC',
      content: 'Just Talking With Fans',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 6,
      image: stream2,
      avatar: avatar2,
      mainName: 'LunaMa',
      content: 'CS-GO 36 Hours Live Stream',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 7,
      image: stream3,
      avatar: avatar3,
      mainName: 'Areluwa',
      content: 'Maybe Nathej Allnight Chillin',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
    {
      id: 8,
      image: stream4,
      avatar: avatar4,
      mainName: 'GangTm',
      content: 'Live Streaming Till Morning',
      status: 'Live',
      viewer: '1.2K',
      games: 'CS-GO'
    },
  ],
  howtostartLiveStream : [
    {
      id: 1,
      image: service1,
      mainName: 'Go To Your Profile',
      content: 'Cyborg Gaming is free HTML CSS website template provided by TemplateMo. This is Bootstrap v5.2.0 layout.'
    },
    {
      id: 2,
      image: service2,
      mainName: 'Live Stream Button',
      content: 'If you wish to support us, you can make a small contribution via PayPal to info [at] templatemo.com'
    },
    {
      id: 3,
      image: service3,
      mainName: 'You Are Live',
      content: 'You are not allowed to redistribute this templates downloadable ZIP file on any other template collection website.'
    }
  ]
}

const MostPopularLiveStream = (state = initialState, action) => {
  switch(action.type) {
    default: 
    return state;
  }
}
export default MostPopularLiveStream;