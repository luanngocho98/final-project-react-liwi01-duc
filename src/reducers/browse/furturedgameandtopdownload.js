import Featured01 from '../../helper/image/featured-01.jpg'
import Featured02 from '../../helper/image/featured-02.jpg'
import Featured03 from '../../helper/image/featured-03.jpg'
import Game1 from '../../helper/image/game-01.jpg';
import Game2 from '../../helper/image/game-02.jpg';
import Game3 from '../../helper/image/game-03.jpg';
const initialState = {
  featuredGames: [
    {
      id: 1,
      image: Featured01,
      mainName: 'CS-GO',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
    {
      id: 2,
      image: Featured02,
      mainName: 'Gamezer',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
    {
      id: 3,
      image: Featured03,
      mainName: 'Island Rusty',
      branchName: '249K Downloaded',
      voteStar: '4.8',
      qualityDownload: '2.3M',
      viewer: '2K4 Streaming'
    },
  ],
  topDownloaded: [
    {
      id: 1,
      image: Game1,
      mainName: 'Fortnite',
      branchName: 'Sandbox',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 2,
      image: Game2,
      mainName: 'CS-GO',
      branchName: 'Legendary',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 3,
      image: Game3,
      mainName: 'PugG',
      branchName: 'Battle Royale',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
  ] ,
  topDownloadedviewmore: [
    {
      id: 1,
      image: Game1,
      mainName: 'Fortnite',
      branchName: 'Sandbox',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 2,
      image: Game2,
      mainName: 'CS-GO',
      branchName: 'Legendary',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 3,
      image: Game3,
      mainName: 'PugG',
      branchName: 'Battle Royale',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 6,
      image: Game1,
      mainName: 'Fortnite',
      branchName: 'Sandbox',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 5,
      image: Game2,
      mainName: 'CS-GO',
      branchName: 'Legendary',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
    {
      id: 4,
      image: Game3,
      mainName: 'PugG',
      branchName: 'Battle Royale',
      voteStar: '4.9',
      qualityDownload: '2.2M'
    },
  ] ,

}

const GameInfo = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
} 

export default GameInfo;