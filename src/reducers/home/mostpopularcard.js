
import {GET_MOSTPOPULAR_CARD_HOME} from '../../services/home/mostpopolarhome/constant'
const initialState = {
  mostpopularhomecard: {
    id: null,
    title: '',
    subTitle: '',
    download: '',
    rate: '',
    imageUrl: ''
  }
};

const mostPopularCardHome = (state = initialState, action) => {
  switch(action.type) {
    case GET_MOSTPOPULAR_CARD_HOME: {
      return {
        ...state,
        mostpopularhomecard: action.payload.data
      }
    }
    default:
      return state;
  }
}
export default mostPopularCardHome;