const initialState = {
  contentBox : [
    {
      name: 'Games Downloaded',
      number: '3',
      id: 1
    },
    {
      name: 'Friends Online',
      number: '16',
      id: 2
    },
    {
      name: 'Live Streams',
      number: 'None',
      id: 3
    },
    {
      name: 'Clips',
      number: '29',
      id: 4
    },
  ]
}

const ContentBox = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}
export default ContentBox;