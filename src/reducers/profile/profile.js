import Clip1 from "../../helper/image/clip-01.jpg";
import Clip2 from "../../helper/image/clip-02.jpg";
import Clip3 from "../../helper/image/clip-03.jpg";
import Clip4 from "../../helper/image/clip-04.jpg";
const initialState = {
  profileDetails: [
    {
      picture: Clip1,
      name: "First Clip",
      quality: "250",
    },
    {
      picture: Clip2,
      name: "Second Clip",
      quality: "183",
    },
    {
      picture: Clip3,
      name: "Third Clip",
      quality: "141",
    },
    {
      picture: Clip4,
      name: "Fourth Clip",
      quality: "91",
    },
  ],
  profileDetailsviewall: [
    {
      picture: Clip1,
      name: "First Clip",
      quality: "250",
    },
    {
      picture: Clip2,
      name: "Second Clip",
      quality: "183",
    },
    {
      picture: Clip3,
      name: "Third Clip",
      quality: "141",
    },
    {
      picture: Clip4,
      name: "Fourth Clip",
      quality: "91",
    },
    {
      picture: Clip1,
      name: "First Clip",
      quality: "250",
    },
    {
      picture: Clip2,
      name: "Second Clip",
      quality: "183",
    },
    {
      picture: Clip3,
      name: "Third Clip",
      quality: "141",
    },
    {
      picture: Clip4,
      name: "Fourth Clip",
      quality: "91",
    },
    {
      picture: Clip1,
      name: "First Clip",
      quality: "250",
    },
    {
      picture: Clip2,
      name: "Second Clip",
      quality: "183",
    },
    {
      picture: Clip3,
      name: "Third Clip",
      quality: "141",
    },
    {
      picture: Clip4,
      name: "Fourth Clip",
      quality: "91",
    },
  ],
};

const ProfileDetails = (state = initialState, action) => {
  switch(action.type) {
    default:
      return state;
  }
};

export default ProfileDetails;
