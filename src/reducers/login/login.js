import { LOGIN_ACTIONS, LOGIN_TYPE } from "../../actions";

const initialState = {
  isOpen: false,
  dialogType: LOGIN_TYPE,
};

const loginReducer = (state = initialState, action) => {
  switch(action.type) {
    case LOGIN_ACTIONS.OPEN:
      return {
        ...state,
        isOpen: true
      }
    case LOGIN_ACTIONS.CLOSE:
      return {
        ...state,
        isOpen: false
      }
    default:
      return state;
  }
}
export default loginReducer;