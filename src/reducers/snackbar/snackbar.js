import { SNACKBAR_ACTIONS } from "../../actions";

const initialState = {
  open: false,
  message: '',
};

const snackbarReducer = (state = initialState, action) => {
  switch(action.type) {
    case SNACKBAR_ACTIONS.OPEN:
      return {
        open: true,
        message: action.message,
      }
    case SNACKBAR_ACTIONS.CLOSE:
      return {
        ...state,
        open: false,
      }
    default:
      return state;
  }
}

export default snackbarReducer;
