import { combineReducers } from "redux";
import authenReducer from './authen.js'
import mostPopularCardHome from './home/mostpopularcard.js';
//home

import GameInfo from './browse/furturedgameandtopdownload';
import MostPopularLiveStream from "./browse/livestreampopular.js";
//browse

import FortNite from "./details/fortnitedetails.js";
import OtherRelatedGames from "./details/othergames.js";

//details

import LiveStreams from "./streams/livestream.js";
import TopStreamers from "./streams/topstream.js";
import StreamsMostPopular from "./streams/streams-popular.js";
//stream

import ProfileDetails from "./profile/profile.js";
import ContentBox from "./profile/contentbox.js";
//profiles

import YourGamingLibrary from "./yourgaminglibrary.js";
//Shared

import loginReducer from "./login/login.js";
import snackbarReducer from "./snackbar/snackbar.js";
//Login


const rootReducer = combineReducers({
  authen: authenReducer,
  game: GameInfo,
  mostpopolarcard: mostPopularCardHome,
  profile: ProfileDetails,
  gaminglibrary: YourGamingLibrary,
  contentbox: ContentBox,
  livestream: MostPopularLiveStream,
  fortnitedetails: FortNite,
  otherrelatedgame: OtherRelatedGames,
  topstreamers: TopStreamers,
  streamsmostpulular: StreamsMostPopular,
  live: LiveStreams,
  login: loginReducer,
  snackbar: snackbarReducer,
})
export default rootReducer;