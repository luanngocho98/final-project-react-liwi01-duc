import axios from "axios";


const config = (isFormData = false) => {
	// const token = localStorage.getItem('access_token');
	let contentType = 'application/json';
	if(isFormData) {
	  contentType = 'multipart/form-data'
	}
	const headers = {
	  headers: {
		'Content-Type': contentType,
		// 'Authorization': `Bearer ${token}`
	  }
	};
}
export const getYourGamingLibrary = (URL, responseCb, errorCb ) => {
	axios.get(URL).then(responseCb).catch(errorCb)
}