export const LOGIN_ACTIONS = {
  OPEN: 'OPEN',
  CLOSE: 'CLOSE',
};

export const LOGIN_TYPE = {
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
};

export const SNACKBAR_ACTIONS = {
  OPEN: 'OPEN_SNACKBAR',
  CLOSE: 'CLOSE_SNACKBAR',
};
